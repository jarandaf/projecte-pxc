var loading = '/img/loadingVideo.gif';
var paused = '/img/loadingVideo.gif';
var notransmitting = '/img/nocam.gif';

//Viewing rooms
var VRooms = function(){
	this.poss = [null, null, null, null];
	this.img = [$('#vvimg0'), $('#vvimg1'), $('#vvimg2'), $('#vvimg3')];
	this.titles = [$('#vvroom0 .vvtitle'), $('#vvroom1 .vvroomcontainer .vvtitle'), $('#vvroom2 .vvroomcontainer .vvtitle'), $('#vvroom3 .vvroomcontainer .vvtitle')];
	this.options = [$('#vvroom0 .vvoptions'), $('#vvroom1 .vvroomcontainer .vvoptions'), $('#vvroom2 .vvroomcontainer .vvoptions'), $('#vvroom3 .vvroomcontainer .vvoptions')];
	this.options_extra = [$('#vvroom0_extraoptions'),$('#vvroom1_extraoptions'),$('#vvroom2_extraoptions'),$('#vvroom3_extraoptions')];
	this.counters = [$('#vvroom1 .vvroomcontainer .vvoptions .option_msg'), $('#vvroom2 .vvroomcontainer .vvoptions .option_msg'), $('#vvroom3 .vvroomcontainer .vvoptions .option_msg')];
	this.close = [$('#vvroomclose1'), $('#vvroomclose2'), $('#vvroomclose3')];
	this.occuped = 0;
}

VRooms.prototype.isFull = function(){
	return this.occuped >=4;
}

VRooms.prototype.viewRoom = function (p) {
    if (p > 0 && p) {
        if (this.poss[0] !== null && this.poss[p] !== null) {
            var t = this.poss[p];
            this.poss[p] = this.poss[0];
            this.poss[0] = t;

            this.poss[0].setpos(0);
            this.poss[p].setpos(p);

            this.setRoom(0);
            this.setRoom(p);
            this.counters[p - 1].html(0);

            this.resetChat();
        }
    }
}

VRooms.prototype.setRoom = function (p) {
	if(typeof p!=='undefined')
    if (this.poss[p] !== null) {
        this.titles[p].html('<span class="user">' + this.poss[p].author + '</span>' + this.poss[p].title);
		this.setEOptions(p);
        this.options[p].show();
        this.img[p].attr('src', this.poss[p].lastframe);
        if (p > 0) {
            this.close[p - 1].show();
        }
    } else {
        this.titles[p].html('NO transmitting');
        this.options[p].hide();
        this.img[p].attr('src', notransmitting);
        if (p > 0) {
            this.close[p - 1].hide();
        }
    }

}

VRooms.prototype.setEOptions = function (p) {
	if(typeof p!=='undefined') {
		console.log('setOptions '+p);
		this.options_extra[p].html('');
		if(this.poss[p]!=null && I.username!=null) {
			this.options_extra[p].append('<a class="send_token" data-username="'+this.poss[p].author+'" data-user="'+this.poss[p].user+'" href="#sendtoken_'+this.poss[p].author+'"></a>');
			if(!following(this.poss[p].author)){
				this.options_extra[p].append('<a class="follow_link" data-username="'+this.poss[p].author+'" href="#follow_'+this.poss[p].author+'"></a>');
			} else {
				this.options_extra[p].append('<a class="unfollow_link" data-username="'+this.poss[p].author+'" href="#unfollow_'+this.poss[p].author+'"></a>');
			}
			if(this.poss[p].play){
				if(this.poss[p].payp2p > 0)  {
					this.options_extra[p].append('<a class="p2p_link_pay" data-room="'+this.poss[p].id+'" href="#p2p_'+this.poss[p].id+'"></a>');
				} else {
					this.options_extra[p].append('<a class="p2p_link_free" data-room="'+this.poss[p].id+'" href="#p2p_'+this.poss[p].id+'"></a>');
				}
			} else {
				if(this.poss[p].payp2p > 0) {
					this.options_extra[p].append('<a class="p2p_link_pay_dissabled" href="#p2p_paused"></a>');
				} else {
					this.options_extra[p].append('<a class="p2p_link_free_dissabled" href="#p2p_paused"></a>');
				}
			}
		}
	}
}
		
VRooms.prototype.resetOptions = function () {
	console.log('resetOptions');
    for(var i = 0; i<4; i++) {
		if (this.poss[i] !== null) {
			this.setEOptions(i);
		}
	}
}
		
VRooms.prototype.moveRoom = function (o, d) {
    if (o != d && o >= 0 && o < 4 && d >= 0 && d < 4) {
        if (this.poss[o] !== null) {
            if (d > 0 && o > 0) {
                 this.counters[d - 1].html(this.counters[o - 1].html());
                 this.counters[o - 1].html(0);
            }else{
                if(o>0)
                    this.counters[o - 1].html(0);
            }
            this.poss[d] = this.poss[o];
            this.poss[o] = null;

            this.poss[d].setpos(d);

            this.setRoom(o);
            this.setRoom(d);
        }
    }
}

VRooms.prototype.deleteRoom = function (p) {
    if (p!==null && p >= 0 && p < 4 && this.poss[p] !== null) {
        this.poss[p].setpos(null);
        this.poss[p] = null;
        this.setRoom(p);
        for (var i = p + 1; i < this.occuped; i++) {
            this.moveRoom(i, i - 1);
        }
        this.occuped--;
    }
	if (p!==null && p == 0) {
        this.resetChat();
    }
    if (this.occuped <= 0) {
        this.hide();
    }
}

VRooms.prototype.newFrame = function(p, frame){
	if(p!==null && p>=0 && p<4 && this.poss[p]!==null){
		this.img[p].attr('src',frame);
	}
}

VRooms.prototype.show = function(){
	$('#viewingTabLink').addClass('active').removeClass('hide');
	$('#camsTabLink, #myCamTabLink').removeClass('active');
	
	$('.tab-pane').removeClass('active');
	$('#tabViewing').addClass('active');
	
	
	this.img = [$('#vvimg0'), $('#vvimg1'), $('#vvimg2'), $('#vvimg3')];
	this.titles = [$('#vvroom0 .vvtitle'), $('#vvroom1 .vvroomcontainer .vvtitle'), $('#vvroom2 .vvroomcontainer .vvtitle'), $('#vvroom3 .vvroomcontainer .vvtitle')];
	this.options = [$('#vvroom0 .vvoptions'), $('#vvroom1 .vvroomcontainer .vvoptions'), $('#vvroom2 .vvroomcontainer .vvoptions'), $('#vvroom3 .vvroomcontainer .vvoptions')];
	this.options_extra = [$('#vvroom0_extraoptions'),$('#vvroom1_extraoptions'),$('#vvroom2_extraoptions'),$('#vvroom3_extraoptions')];
	this.counters = [$('#vvroom1 .vvroomcontainer .vvoptions .option_msg'), $('#vvroom2 .vvroomcontainer .vvoptions .option_msg'), $('#vvroom3 .vvroomcontainer .vvoptions .option_msg')];
    this.close = [$('#vvroomclose1'), $('#vvroomclose2'), $('#vvroomclose3')];
}

VRooms.prototype.hide = function(){
	$('#viewingTabLink').removeClass('active').addClass('hide');
	$('#camsTabLink').addClass('active');
	
	$('.tab-pane').removeClass('active');
	$('#tabCams').addClass('active');
}

VRooms.prototype.resetChat = function () {
    $('#vvroomchatcontainer').html('');
    if (this.poss[0] !== null) {
        for (line in this.poss[0].chat) {
            this.chatMsg(this.poss[0].chat[line]);
        }
    }
}

VRooms.prototype.chatMsg = function (msg) {
    $('#vvroomchatcontainer').append('<div class="chatmsg_'+msg.level+'"><span class="chatmsg_username">' + msg.user + ' : </span>' + msg.msg + '</div>');
}

VRooms.prototype.chatMsgWait = function (p) {
     if (p > 0 && p<4) {
         this.counters[p - 1].html(parseInt(this.counters[p - 1].html())+1);
     }
}

VRooms.prototype.setTitle = function(p, author, title) {
	this.titles[p].html(author+' : '+title);
}

VRooms.prototype.sendChat = function (msg) {
    if (this.poss[0] !== null) {
        this.poss[0].sendChat(msg);
    }
}


VRooms.prototype.getRoom = function(vroom){
	for(var i = 0; i<4; i++){
		if(this.poss[i]===null){ 
			this.poss[i] = vroom; 
			this.occuped++;
			return i;
		};
	}
	return null;
}

VRooms.prototype.transmitting = function (room) {
    for (var i = 0; i < 4; i++) {
        if (this.poss[i] !== null) {
            if (this.poss[i].id == room)
                return true;
        };
    }
    return false;
}

var vrooms = new VRooms();

//Room
var VRoom = function (id) {
    this.id = id;
    this.pos = null;
    this.status = false;
    this.socket = null;
    this.data = null;
    this.chat = new Array();
    this.author = "";
	this.user = null;
    this.title = "";
    this.lastframe = loading;
    this.play = false;
    this.payp2p = false;
}

VRoom.prototype.setpos = function(p){
	this.pos = p;
}

VRoom.prototype.startViewing = function (data) {
    var room = this;

    this.data = data;
    this.author = data.creator;
    this.title = data.roomName;
    this.user = data.userRoom;
    this.payp2p = data.payp2p;

	var p = vrooms.getRoom(this);
    this.pos = p;
    if (this.pos === null) {
        console.log('Too much rooms viewing');
        return null;
    }

    this.socket = io.connect(data.url, { 'force new connection': true });
    console.log('Connect to ' + data.url);

    this.socket.emit('ini', data);

    this.socket.on('error', function (reason) {
        room.close(reason);
    });
    this.socket.on('nack', function (reason) {
        room.close(reason);
    });
    this.socket.on('RoomClose', function () {
        room.close('Session Ended');
    });
    this.socket.on('ack', function () {
        room.status = true;
    });

    this.socket.on('newFrame', function (data) {
		if(!room.play) {
			room.play = true;
			vrooms.setEOptions(room.pos);
		}
        room.newFrame(data);
    });

    this.socket.on('paused', function () {
		if(room.play) {
			room.play = false;
			vrooms.setEOptions(room.pos);
			room.newFrame(paused);
		}
    });

    this.socket.on('chatMsg', function (data) {
        room.chatMsg(data);
    });
	
	return p;
};

VRoom.prototype.close = function(reason){
	newAlert('warning', 'Room closed : '+reason);
	delete this.socket;
	delete this.chat;
	vrooms.deleteRoom(this.pos);
	delete this;
}

VRoom.prototype.closeRoom = function(){
	this.socket.disconnect();
}

VRoom.prototype.newFrame = function (frame) {
    this.lastframe = frame;
    vrooms.newFrame(this.pos, frame);
}

VRoom.prototype.sendChat = function (msg) {
    this.socket.emit('chatMsg', msg);
}

VRoom.prototype.chatMsg = function (msg) {
    this.chat.push(msg);
    if (this.pos === 0) {
        vrooms.chatMsg(msg);
    } else {
        vrooms.chatMsgWait(this.pos);
    }
}


function startViewing(data) {
	vrooms.show();
    vroom = new VRoom(data.room);
    var p = vroom.startViewing(data);
	console.log(p);
	if(typeof p != 'undefined')
		vrooms.setRoom(p);
}

$('#vvroomchatform').live('submit', function (e) {
    e.preventDefault();
    var v = $('#vvroomchatinput').val();
    if (v != "") {
        vrooms.sendChat(v);
    }
    $('#vvroomchatinput').val('');
});

$('#option_zoom1').live('click', function (e) {
    e.preventDefault();
    vrooms.viewRoom(1);
});

$('#option_zoom2').live('click', function (e) {
    e.preventDefault();
    vrooms.viewRoom(2);
});

$('#option_zoom3').live('click', function (e) {
    e.preventDefault();
    vrooms.viewRoom(3);
});


$('#vvroomclose0').live('click', function (e) {
    e.preventDefault();
    vrooms.deleteRoom(0);
});

$('#vvroomclose1').live('click', function (e) {
    e.preventDefault();
    vrooms.deleteRoom(1);
});

$('#vvroomclose2').live('click', function (e) {
    e.preventDefault();
    vrooms.deleteRoom(2);
});

$('#vvroomclose3').live('click', function (e) {
    e.preventDefault();
    vrooms.deleteRoom(3);
});