var dataSocket;
var trs = 0;
var rooms = 0;
var I = {
	user : null,
	username : null,
	lastRoomClicked: null,
	lastRoomTokens: null,
	lastError: null,
	follow : new Array(),
	tokens: null
};

var alerts = 0;

var p2p = {
	// marca si es el que inicia el p2p o no
	initiator: false,
	// un usuari nomes pot tenir un p2p
	status: "free",
	// guarda els candidates del iniciador per enviar quan hagi rebut el response
	candidates: [],
	// guarda l'objecte
	peerConnection: null,
	// mira si s'ha rebut el response o no
	responseReceived: false,
	// amb qui estas fent el p2p
	p2pwith_username: null,
	p2pwith: null
}

$(function () {
	//My modal call
	$('.my_modal').live('click', function (e) {
		e.preventDefault();
		var url = $(this).attr('href');
		$.get(url, function (data) {
			$('#modal_container').html(data);
			$('#modal_container').modal();
		}).success(function () { $('input:text:visible:first').focus(); });
	});

	//dataSocket
	//dataSocket connect
	if(I.user == null) dataSocket = io.connect('https://' + window.document.location.host, { 'force new connection': true });

	//dataSocket disconnect
	dataSocket.on('disconnect', function () {
		newAlert('error', 'Disconnected from server');
		location.reload(true);
	});

	//dataSocket connect ACK
	dataSocket.on('connected', function (data) {
		console.log(data);
		keepAlive = setInterval(function () {
			dataSocket.emit('keepAlive');
		}, 2000);
		I.user = data.user;
		$.ajax({
			url: '/logued.html',
			success: function (data) {
				$('body').html(data);
			},
			async: false
		});
		
		dataSocket.on('keepAlive',function(){});

		updateRooms(data.rooms);
		startDataSocket(dataSocket);
	});



	//On close close all
	$(window).unload(function () {
		dataSocket.disconnect();
		if (croom !== null) {
			croom.closeRoom();
		}
	});
});

function startDataSocket(dataSocket){
//Login
	//doLogIn
	function logFun() {
		if(I.username === null) {
			var username = $('#form_login_username').val();
			var password = $('#form_login_password').val();
			$('#form_login_output').html('');
			
			dataSocket.emit('login', { 
				username:username,
				password:password
			});
		} else {
			$('#modal_container').modal('hide');
		}
	};
	$('#button_login').live('click', function (e){
		console.log('button login click');
		e.preventDefault();
		logFun();
	});
	$('#form_login').live('submit', function (e){
		console.log('form login submit');
		e.preventDefault();
		logFun();
	});
	//Login ACK
	dataSocket.on('login-ack', function (data) {
		$('#form_login_output').html('<p class="text-success"><i class="icon-ok-sign"></i> Welcome '+data.username+'!!</p>');
		//Do login chorradas
		I.username = data.username;
		I.follow = data.follow;
		I.tokens = parseInt(data.tokens);
		console.log("tokens: " + I.tokens);
		if(isNaN(I.tokens)){
			I.tokens = 0;
		}
		resetBadge();
		vrooms.resetOptions();
		$('#myprofile').html(data.username);
		$('#button_container_login').addClass('hide');
		$('#button_container_new_room, #button_container_myprofile, #button_container_logout, #button_container_buycredit').removeClass('hide');
		
		$('#filter_all_container').removeClass('hide');
		
		$('#modal_container').modal('hide');
		newAlert('success', 'Welcome '+data.username+'!!');
	});
	
	function resetBadge(){
		var badgeTokens = $("#myTokensBadge");
		if(I.tokens == 0){
			badgeTokens.removeClass("hide").addClass("badge-important");
		}
		else if(I.tokens > 0 && I.tokens < 50){
			badgeTokens.removeClass("hide").addClass("badge-warning");
		}
		else{
			badgeTokens.removeClass("hide").addClass("badge-success");
		}
		badgeTokens.text(I.tokens);
	}
	
	//Login NACK
	dataSocket.on('login-nack', function (data) {
		I.username = null;
		I.follow = new Array();
		vrooms.resetOptions();
		$('#form_login_output').html('<p class="text-error"><i class="icon-remove-circle"></i> Username or login incorrect.</p>');
	});
	
	
//SignIn
	function signFun() {
		if(I.username === null) {
			var username = $('#form_login_username').val();
			var password = $('#form_login_password').val();
			var password2 = $('#form_login_password2').val();
			$('#form_login_output').html('');
			
			dataSocket.emit('signin', { 
				username:username,
				password:password,
				password2:password2
			});
		} else {
			$('#modal_container').modal('hide');
		}
	};
	$('#button_signin').live('click', function (e){
		console.log('button signin click');
		e.preventDefault();
		signFun();
	});
	$('#form_signin').live('submit', function (e){
		console.log('form signin submit');
		e.preventDefault();
		signFun();
	});
	
	//SignIn ACK
	dataSocket.on('signin-ack', function (data) {
		$('#form_signin_output').html('<p class="text-success"><i class="icon-ok-sign"></i> Welcome '+data.username+'!!</p>');
		//Do login chorradas
		I.username = data.username;
		I.follow = new Array();
		
		I.tokens = 0;
		resetBadge();
		
		
		vrooms.resetOptions();
		$('#myprofile').html(data.username);
		$('#button_container_login').addClass('hide');
		$('#button_container_new_room, #button_container_myprofile, #button_container_logout').removeClass('hide');
		
		$('#openRooms, #closedRooms').addClass('hide');
		$('#filter_all_container').removeClass('hide');
		
		$('#modal_container').modal('hide');
		
		newAlert('success', 'Welcome '+data.username+'!!');
		
	});
	
	//SignIn NACK
	dataSocket.on('signin-nack', function (data) {
		console.log('error reg');
		I.username = null;
		I.follow = new Array();
		vrooms.resetOptions();
		$('#form_signin_output').html('<p class="text-error"><i class="icon-remove-circle"></i> '+data.msg+'</p>');
	});
	
	
	//doLogOut
	$('#button_logout').live('click', function (e) {
		e.preventDefault();
		I.username = null;
		I.follow = new Array();
		
		I.tokens = null;
		//resetBadge();
		
		vrooms.resetOptions();
		
		dataSocket.emit('logout');
		
		$('#myprofile').html('');
		$('#button_container_new_room, #button_container_myprofile, #button_container_logout').addClass('hide');
		$('#button_container_login').removeClass('hide');
		var myTokensBadge = $('#myTokensBadge');
		switch(myTokensBadge.attr('class')){
			case "badge badge-warning":
				myTokensBadge.removeClass('badge-warning');
				break;
			case "badge badge-success":
				myTokensBadge.removeClass('badge-success');
				break;
			case "badge badge-important":
				myTokensBadge.removeClass('badge-important');
				break;
			default: break;
		}
		myTokensBadge.addClass('hide');
		//window.location.reload();
		$('#filter_all').attr('checked', true)
		$('#openRooms, #closedRooms').removeClass('hide');
		$('#filter_all_container').addClass('hide');
	});


	$('.filter').live('click', function (e){
		console.log($(this).val());
		if($(this).is(':checked')) {  
			$('.gen_'+$(this).val()).removeClass('shide');
		} else {
			$('.gen_'+$(this).val()).addClass('shide');
		}
	});
	$('#filter_all').live('click', function (e){
		if($(this).is(':checked')) {  
			$('#openRooms, #closedRooms').removeClass('hide');
		} else {
			$('#openRooms, #closedRooms').addClass('hide');
		}
	});
	
	
	//dataSocket lista de salas
	dataSocket.on('list', function (data) {
		updateRooms(data);
	});
	
	//dataSocket lista de salas
	dataSocket.on('update', function (data) {
		updateRoom(data);
	});
	
	
	$('.send_token').live('click', function (e){
		console.log('send_token click');
		e.preventDefault();
		var tokens = prompt("How much tokens do you want to send to "+$(this).attr('data-username')+"?");
		if(tokens != null) {
			dataSocket.emit('sendTokens',{userto:$(this).attr('data-user'), usernameto:$(this).attr('data-username'), tokens : tokens});
		}
	});
	
	dataSocket.on('sendTokens-ack', function (data) {
		newAlert('success', 'Sended '+data.tokens+' tokens to '+data.to);
		I.tokens = parseInt(I.tokens) - parseInt(data.tokens);
		$("#myTokensBadge").text(I.tokens);
		updateBadgeStatus();
	});
	
	dataSocket.on('sendTokens-nack', function (data) {
		newAlert('error', 'Sending tokens :'+data.msg);
	});
	
	dataSocket.on('receivedTokens-ack', function (data) {
		newAlert('success', 'Received '+data.tokens+' tokens from '+data.from);
		I.tokens = parseInt(I.tokens) + parseInt(data.tokens);
		$("#myTokensBadge").text(I.tokens);
		updateBadgeStatus();
	});
	
	

	//dataSocket new room
	function startTransmissionFun() {
		if (trs == 0) {
			trs = 1;
			var roomName = $("#new_room_name").val();
			var gen = $("#new_room_gen").val();
			var frameRate = $("#new_room_framerate").val();
			var allowUnregistered = $("#new_room_allow_unregistered").val();
			var tokens = $("#new_room_tokens").val();
			var payp2p = $("#new_room_payp2p").val();
			if(tokens == "") tokens = 0;
			if(payp2p == "") payp2p = 0;
			//miro quina pestanya del tab està activa, si és la corresponent a sala privada he d'agafar també el password
			var password = $("#new_room_password").val();  
			if(typeof password === 'undefined')
				password = "";
			
			var type;
			if(password=="") type = 'public';
			else type = 'private';
			if(tokens != "") type += ("_token");
			console.log("Tipus de sala:" + type);
			dataSocket.emit('newRoom', {user: I.user, username: I.username, roomName: roomName, gen: gen, frames: frameRate, allowUnregistered: allowUnregistered, tokens: tokens, password: password, type: type, payp2p:payp2p});
			$('#newRoomErrorMsg').html('');
		}
	}
	
	$('#startTransmission').live('click', function (e) {
		e.preventDefault();
		startTransmissionFun();
	});
	$('#form_startTransmission').live('submit', function (e) {
		e.preventDefault();
		startTransmissionFun();
	});

	$('.room_closed').live('click', function (e){
		e.preventDefault();
	});

	$('.room_created').live('click', function (e){
		e.preventDefault();
		var room = $(this).attr('href').match(/r=([^&]+)/)[1];
		var tokens = $(this).attr('data-tokens');
		I.lastRoomClicked = room;
		I.lastRoomTokens = tokens;
	});

	$('#button_join_room').live('click', function (e){
		e.preventDefault();
		var room = $(this).attr("data-room");
		var type = $(this).attr("data-modal");
		var password = "";
		var tokens = 0;
		if(type == "private_token"){
			password = $("#join_room_password").val();
			tokens = I.lastRoomTokens;
		}
		else if(type == "private"){
			password = $("#join_room_password").val();
		}
		else if(type == "public_token"){
			tokens = I.lastRoomTokens;
		}
		$("#modal_container").modal('hide');
		//només en cas que tinguem prous tokens enviem l'event, altrament l'ignorem
		if(I.tokens >= I.lastRoomTokens) dataSocket.emit('joinRoom', {room: room, password: password, user: I.username});
	});

	$('.follow_link').live('click', function (e){
		e.preventDefault();
		var username = $(this).attr('data-username');
		dataSocket.emit('follow', username);
	});
	$('.unfollow_link').live('click', function (e){
		e.preventDefault();
		var username = $(this).attr('data-username');
		dataSocket.emit('unfollow', username);
	});

	$('#button_tokens_sale').live('click', function (e){
		e.preventDefault();
		var numTokens = $('#num_tokens').val();
		console.log('you have chose ' + numTokens + ' tokens');
		$("#modal_container").modal('hide');
		dataSocket.emit('doPayment', {user: I.user, username: I.username, amount: numTokens});
	});
	
	$('.p2p_link_pay, .p2p_link_free').live('click', function (e){
		e.preventDefault();
		console.log("start p2p req");
		var room = $(this).attr('data-room');
		
		p2p.initiator = false;
		$('#p2ptitle').html('Waiting for P2P');
		$('#p2p').show();
		$("#web_container").hide();
		p2p.status = "waiting";
		
		dataSocket.emit('p2pAsk', {room:room});
	});
	
	dataSocket.on('p2pAsk-nack', function (data) {
		p2p.status = "free";
		$("#p2p").hide();
		$("#web_container").show();
		newAlert('warning', 'P2P refused:'+data.msg);
	});
	
	dataSocket.on('p2pAsk-ack', function (data) {
		p2p.initiator = false;
		p2p.p2pwith = data.p2pwith;
		p2p.p2pwith_username = data.username;
		$('#p2ptitle').html('P2P with '+data.username);
		$('#p2p_send_token').attr('data-username', p2p.p2pwith_username).attr('data-user', p2p.p2pwith);
		$('#p2p').show();
		$("#web_container").hide();
		p2p.status = "waiting";
		newAlert('success', 'P2P accepted');
	});
	
	dataSocket.on('p2pAsk', function (data) {
		console.log("receive p2p req");
		if(p2p.status == "free"){
			var resp = confirm('P2P request from '+data.username);
			if(resp) {
				dataSocket.emit('p2pAsk-ack', {p2pwith:data.p2pwith});
				
				p2p.initiator = true;
				p2p.p2pwith = data.p2pwith;
				p2p.p2pwith_username = data.username;
				$('#p2ptitle').html('P2P with '+p2p.p2pwith_username);
				$('#p2p_send_token').attr('data-username', p2p.p2pwith_username).attr('data-user', p2p.p2pwith);
				$("#web_container").hide();
				$('#p2p').show();
				p2p.status = "ok";
				p2pInitiator();
			} else {
				dataSocket.emit('p2pAsk-nack', {p2pwith:data.p2pwith});
			}
		}else{
			dataSocket.emit('p2pAsk-nack', {p2pwith:data.p2pwith});
		}
	});
	
	dataSocket.on('p2p-offer', function (data){
	
		if (p2p.status == "waiting"){
			p2p.status = "ok";
			p2p.initiator = false;
			p2pOffer(data);
		}
	});

	// al rebre una reponsta de la offer
	dataSocket.on('p2p-response', function(data) {
		if (p2p.status == "ok"){
			p2pResponse(data);
		}
	});

	//si rebem un candidate l'afegim al peerconnection
	dataSocket.on('p2p-candidate', function(data) {
		if (p2p.status == "ok"){
			p2pOnIceCandidaterc(data);
		}
	});

	// acabem el p2p
	dataSocket.on('p2pClose', function(data) {
		if (p2p.status == "ok"){
			console.log("stop p2p");
			p2p.status = "free";
			if(p2p.peerConnection!=null){
				p2p.peerConnection.close();
			}
			p2p.peerConnection = null;
			$("#p2p").hide();
			$("#web_container").show();
		}
	});
	
	$('#p2p_close').live('click', function (e){
		console.log("stop p2p");
		p2p.status = "free";
		if(p2p.peerConnection!=null){
			p2p.peerConnection.close();
		}
		p2p.peerConnection = null;
		$("#p2p").hide();
		$("#web_container").show();
		dataSocket.emit('p2pClose');
	});
	
	//dataSocket new room ACK
	dataSocket.on('newRoom-ack', function (data) {
		setTimeout(function(){
			$('#modal_container').modal('hide');
		},1000);
		if (trs == 1) {
			console.log('dataProxy dice de iniciar la retrans');
			console.log('url que me pasa: ' + data.url);
			startTransmission(data);
		}
	});

	//dataSocket new room NACK
	dataSocket.on('newRoom-nack', function (data) {
		trs = 0;
		$('#newRoomErrorMsg').html('<p class="text-error">An error ocurred.</p>');
	});
	
	//dataSocket new room NACK
	dataSocket.on('newRoom-check', function (data) {
		$('#new_room_name_error').html('');
		$('#new_room_tokens_error').html('');
		$('#new_room_p2p_error').html('');
		$('#new_room_gen_error').html('');
		trs = 0;
		console.log(data)
		for (error in data){
			switch(data[error]) {
				case 'neededRoom':
					$('#new_room_name_error').html('<p class="text-error">The name of the room is required.</p>');
					break;
				case 'invalidTokens':
					$('#new_room_tokens_error').html('<p class="text-error">Invalid number of tokens.</p>');
					break;
				case 'invalidPayp2p':
					$('#new_room_p2p_error').html('<p class="text-error">Invalid number of tokens to get p2p.</p>');
					break;
				case 'invalidGen':
					$('#new_room_gen_error').html('<p class="text-error">Invalid gender.</p>');
					break;
			}
		}
	});
	
	dataSocket.on('joinRoom-ack', function (data) {
		newAlert('success', 'Joining to room');
		startViewing(data);
	});
	
	//dataSocket joinRoom NACK
	dataSocket.on('joinRoom-nack', function (data) {
		newAlert('error', 'Joining to room: '+data.msg);
	});
	
	//dataSocket Follow NACK
	dataSocket.on('follow-nack', function (data) {
		newAlert('error', 'Following: '+data.msg);
	});
	
	//dataSocket unFollow NACK
	dataSocket.on('unfollow-nack', function (data) {
		newAlert('error', 'Unfollowing: '+data.msg);
	});
	
	//dataSocket Follow ACK
	dataSocket.on('follow-ack', function (data) {
		newAlert('success', 'Following: '+data.follow);
		if(!following(data.follow)) {
			I.follow.push(data.follow);
			vrooms.resetOptions();
		}
	});
	
	//dataSocket unFollow ACK
	dataSocket.on('unfollow-ack', function (data) {
		newAlert('warning', 'Stopped Following: '+data.follow);
		var u = following(data.follow);
		if(u!==false) {
			delete I.follow[u];
			vrooms.resetOptions();
		}
	});

	dataSocket.on('incTokens-ack', function (data){
		newAlert('success', 'Added '+data.tokens+' tokens');
		I.tokens = parseInt(I.tokens) + parseInt(data.tokens);
		$("#myTokensBadge").text(parseInt(I.tokens));
		updateBadgeStatus();
	});

	dataSocket.on('incTokens-nack', function (data){
		newAlert('error', 'Adding tokens: '+data.msg);
	});

	dataSocket.on('decTokens-ack', function (data){
		newAlert('warning', 'Decremented '+data.tokens+' tokens');
		I.tokens = parseInt(I.tokens) - parseInt(data.tokens);
		$("#myTokensBadge").text(I.tokens);
		updateBadgeStatus();
	});

	dataSocket.on('decTokens-nack', function (data){
		newAlert('error', 'Decrementing tokens: '+data.msg);
	});

	dataSocket.on('paypalRedirection', function (data){
		window.open(data.paymentUrl,'_blank');
		window.focus(); //hack para abrir nuevo tab
	});

	function updateBadgeStatus(){
		var myTokensBadge = $("#myTokensBadge");
		console.log('current tokens: ' + parseInt(myTokensBadge.text()));
		if(parseInt(myTokensBadge.text()) == 0){
			if(myTokensBadge.hasClass("badge-success")) myTokensBadge.removeClass("badge-success").addClass("badge-important");
			else if(myTokensBadge.hasClass("badge-warning")) myTokensBadge.removeClass("badge-warning").addClass("badge-important");
		}
		else if(parseInt(myTokensBadge.text()) > 0 && parseInt(myTokensBadge.text()) < 50){
			if(myTokensBadge.hasClass("badge-important")) myTokensBadge.removeClass("badge-important").addClass("badge-warning");
			else if(myTokensBadge.hasClass("badge-success")) myTokensBadge.removeClass("badge-success").addClass("badge-warning");
		}
		else{
			if(myTokensBadge.hasClass("badge-important")) myTokensBadge.removeClass("badge-important").addClass("badge-success");
			else if(myTokensBadge.hasClass("badge-warning")) myTokensBadge.removeClass("badge-warning").addClass("badge-success");
		}
	}
}

//Start data transmission
function startTransmission(data){
	videoCapture(data);
	//Close modal
	$('#modal_container').modal('hide');
	$('#myCam').show();
	trs = 2;
}

//Update Rooms
function updateRooms(data){
	rooms = data;
	$('#openRooms, #closedRooms').html('');
	for(var room in rooms){
		var r = rooms[room];
		if(r.frame!==null && r.frame != 'data:,'){
			if(I.username!=null || r.allowUnregistered) {
				if(r.state=='created' || r.state=='closed'){
					var classes = 'view_room gen_'+rooms[room].gen+' room_type_'+r.type;
					var container = '';
					if(r.state=='created'){
						classes = classes+' my_modal room_created';
						if(following(r.creator)!==false) {
							container = '#rooms #openRooms_follow';
						} else {
							container = '#rooms #openRooms';
						}
					} else {
						classes = classes+' room_closed';
						container = '#rooms #closedRooms';
					}
					$(container).append('<a id="view_room_'+r.id+'" class="'+classes+'" data-tokens="' + r.tokens + '" href="/load/' +r.type + '.html?r=' + rooms[room].id + '">'+
					'<div class="view_room_header"><span class="view_room_header_creator">'+r.creator+'</span><span class="view_room_header_name">'+r.roomName+'</span></div>'+
					'<img class="view_room_img" src="'+r.frame+'" /></a>');
				}
			}
		}
	}
}

//Update Rooms
function updateRoom(r){
	$('#view_room_'+r.id).remove();
	if(r.frame!==null && r.frame != 'data:,') {
		if(I.username!=null || r.allowUnregistered) {
			if(r.state=='created' || r.state=='closed') {
				var classes = 'view_room gen_'+r.gen+' room_type_'+r.type;
				var container = '';
				if(r.state=='created'){
					classes = classes+' my_modal room_created';
					if(following(r.creator)!==false) {
						container = '#rooms #openRooms_follow';
					} else {
						container = '#rooms #openRooms';
					}
				} else {
					classes = classes+' room_closed';
					container = '#rooms #closedRooms';
				}
				$(container).append('<a id="view_room_'+r.id+'" class="'+classes+'" data-tokens="' + r.tokens + '" href="/load/' +r.type + '.html?r=' + r.id + '">'+
				'<div class="view_room_header"><span class="view_room_header_creator">'+r.creator+'</span><span class="view_room_header_name">'+r.roomName+'</span></div>'+
				'<img class="view_room_img" src="'+r.frame+'" /></a>');	
			}
		}
	}
}

function following(follow) {
	for(var u in I.follow) {
		if(typeof I.follow[u]!=='undefined' && I.follow[u] == follow) {
			return u;
		}
	}
	return false;
}

/*A ver que se aprovecha*/
// p2p --------------------------------------------------------------------
// inicialitza una conexió, enviem un offer al altre peer
function p2pInitiator(){
	console.log("p2pInitiator");
	p2pPrepare();
	// capturem la càmara i el video
	navigator.getUserMedia({video: true, audio: true}, function(localMediaStream) { 
		// assignem el video local al tag que toca
		$('#p2p-myVideo').attr('src', window.URL.createObjectURL(localMediaStream));
		// afegim el stream al peer connection
		p2p.peerConnection.addStream(localMediaStream);

			// creem una offer la guardem i la enviem a l'altre peer
			p2p.peerConnection.createOffer(function(data){
				dataSocket.emit('p2p-offer',data);
				p2p.peerConnection.setLocalDescription(data)
			});
	}, function(error) {
		console.log(error);
	});
	
	
}

//quan rebem un offer
function p2pOffer(data){
	console.log('p2poffer', data);
	p2pPrepare();
	//agafem la camara i el video
	navigator.getUserMedia({video: true, audio: true}, function(localMediaStream) {
		console.log("agafant user media");
		//assignem el video local al peerconection
		p2p.peerConnection.addStream(localMediaStream);
		//assignem el video local al tag que toca
		$('#p2p-myVideo').attr('src', window.URL.createObjectURL(localMediaStream));
		//afegim el sdp de l'altre peer
		console.log("data: " + data);
		p2p.peerConnection.setRemoteDescription(new RTCSessionDescription(data));
		//creem una contestacio (sdp) i li enviem
		p2p.peerConnection.createAnswer(function(desc){
			p2p.peerConnection.setLocalDescription(desc);
			dataSocket.emit('p2p-response', desc);
			p2p.peerConnection.onaddstream = p2pRemoteView; 
			
		});
	});
}

//al rebre la contestació del offer que hem enviat
function p2pResponse(data){
	// asignem l'event d'afegir un stream a la funció que li assignarà el recurs on veure'l
	p2p.peerConnection.onaddstream = p2pRemoteView;
	console.log('p2presponse');
	// marquem que ja hem rebut el response
	p2p.responseReceived = true;
	// posem el sdp a la peerconnection
	p2p.peerConnection.setRemoteDescription(new RTCSessionDescription(data));
	// com que ja hem rebut el response enviem els candidates
	for(var i = 0; i<p2p.candidates.length; i++){
   		dataSocket.emit('p2p-candidate', p2p.candidates[i]);
   	}   

}
//prepara el peerconnection per el p2p
function p2pPrepare(){
	//inicialitzem les variables
	p2p.responseReceived = false;
	p2p.candidates = [];
	p2p.peerConnection = null;

	//mirem quina funció fa servir el navegador
	var PeerConnection = window.PeerConnection || window.webkitPeerConnection00 || window.webkitRTCPeerConnection;
	//inicialitzem la conexió amb el servidor stun
	p2p.peerConnection = new PeerConnection({iceServers:[{url:"stun:stun.l.google.com:19302"}]});
	// assignem funció de ice
	p2p.peerConnection.onicecandidate = p2pOnIceCandidate;

	
	//mirem quina funció fa servir el navegador per agafar la càmara
	navigator.getUserMedia = navigator.getUserMedia || navigator.webkitGetUserMedia || navigator.mozGetUserMedia || navigator.msGetUserMedia;
	window.URL = window.URL || window.webkitURL;
}

//quan tenim un ice candidate li enviem a l'altre banda
function p2pOnIceCandidate(event) {
	if (event.candidate) {
			data = {
				type: 'candidate',
				label: event.candidate.sdpMLineIndex,
				id: event.candidate.sdpMid,
				candidate: event.candidate.candidate
			};
		// si som els iniciadors i encara no hem rebut el response esperem per no enviar abans un candidate de que l'altre peer hagi creat en peerConnection
		if (p2p.initiator && !p2p.responseReceived){
			p2p.candidates.push(data)
		}else{
			dataSocket.emit('p2p-candidate',data);
		}
	} else {
	  console.log("End of candidates.");
	}
}

//quan rebem un ice candidate de l'altre banda el posem al peerconnection
function p2pOnIceCandidaterc(data) {
	//console.log(event);
	var candidate = new RTCIceCandidate({sdpMLineIndex:data.label, candidate:data.candidate});
	p2p.peerConnection.addIceCandidate(candidate);
}

//activem la view de la càmara quan arriba el stream
function p2pRemoteView(st) {
	$('#p2p-otherVideo').attr('src', webkitURL.createObjectURL(st.stream));
}

function p2pLocalView(st){
	$('#p2p-myVideo').attr('src', webkitURL.createObjectURL(st.stream));
}


function newAlert(atype, text) {
	var aid = ++alerts;
	switch(atype) {
		case 'success':
			$('#alerts_container').prepend('<div id="alert'+aid+'" class="alert alert-success"><button type="button" class="close" data-dismiss="alert">×</button><strong>Success!</strong> '+text+'</div>');
			break;
		case 'error':
			$('#alerts_container').prepend('<div id="alert'+aid+'" class="alert alert-error"><button type="button" class="close" data-dismiss="alert">×</button><strong>Error!</strong> '+text+'</div>');
			break;
		default:
			$('#alerts_container').prepend('<div id="alert'+aid+'" class="alert alert-block"><button type="button" class="close" data-dismiss="alert">×</button><strong>Warning!</strong> '+text+'</div>');
			break;
	}
	setTimeout(function () {
        $('#alert'+aid).hide(1000, function(){
			$(this).remove();
		});
    }, 5000);
	
	
}
