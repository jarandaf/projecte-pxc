var croom = null;
/* Variables globales */
URL = window.URL || window.webkitURL;
navigator.getUserMedia = navigator.getUserMedia || navigator.webkitGetUserMedia ||navigator.mozGetUserMedia || navigator.msGetUserMedia;

/* Caching de elementos del DOM */
var video = null;
var canvas = null;
var ctx = null;

var CRoom = function (id, roomName) {
    this.id = id;
	this.name = roomName;
	$('#myCamroom .vvtitle').html(roomName);
	
    this.pos = null;
    this.status = false;
    this.socket = null;
    this.data = null;
    this.frames = null;
    this.stream = null;

    this.drawInterval = null;

    croom = this;
}



CRoom.prototype.videoCapture = function (data) {
    this.data = data;
    this.frames = data.frames;
    this.url = data.url;


    var localMediaStream = null;

    console.log('Frames: ' + this.frames);


    if (navigator.getUserMedia) {
        video = document.getElementById('myVideo');
        canvas = document.getElementById('myLocalCanvas');
        ctx = canvas.getContext('2d');
        navigator.getUserMedia({ video: true, audio: false }, function (stream) { croom.gotVideo(stream); }, function (stream) { croom.notGotVideo(e); });
    }
};

CRoom.prototype.gotVideo = function (stream) {
    video.src = URL.createObjectURL(stream);
    this.stream = stream;

    this.startConnection();
}

CRoom.prototype.notGotVideo = function (e) {
    alert('Error al obtener el stream de video.');
}

CRoom.prototype.snapshot = function() {
    
    var cw = 320;
    var ch = 320 * video.clientHeight / video.clientWidth;

    canvas.width = cw;
    canvas.height = ch;

    if (this.stream) {
        ctx.drawImage(video, 0, 0, cw, ch);
		var frame = canvas.toDataURL('image/jpeg', 0.2);
		if(frame!=null && frame != 'data:,')
			this.socket.emit('newFrame', frame);
		else
			this.socket.emit('paused');
    } else
		this.socket.emit('paused');
}

CRoom.prototype.startConnection = function () {
    console.log('url del broadcast: ' + this.url);
    this.socket = io.connect(this.url, { 'force new connection': true });

    this.socket.emit('newRoom', this.data);

    this.socket.on('error', function (reason) {
        console.log('error');
        croom.close(reason);
    });

    this.socket.on('disconnect', function (reason) {
        console.log('disconnect');
        croom.close(reason);
    });


    this.socket.on('newRoom-nack', function (reason) {
        croom.close('Socket refuses connection' + reason);
    });

    this.socket.on('newRoom-ack', function () {
        this.status = true;
        console.log('Vinga, comencem a enviar frames al broadcast!');
        croom.drawInterval = setInterval(function () { croom.snapshot(); }, 1000 / croom.frames);
    });


    this.socket.on('chatMsg', function (data) {
        croom.chatMsg(data);
    });
}

CRoom.prototype.closeRoom = function(){
	this.socket.emit('endconnection',{});
	this.close('I close room');
}


CRoom.prototype.close = function (reason) {

	$('#myCamTabLink').removeClass('active').addClass('hide');
	$('#camsTabLink').addClass('active');
	
	$('.tab-pane').removeClass('active');
	$('#tabCams').addClass('active');

    console.log('close Room : ' + reason);
    if (this.socket !== null) {
        this.socket.disconnect();
    }
    delete this.socket;
    clearInterval(this.drawInterval);
    video.pause();
    video.src=null;
    this.status = false;
    this.resetChat();
    croom = null;
	trs = 0;
}


CRoom.prototype.resetChat = function () {
    $('#myCamroomchatcontainer').html('');
}

CRoom.prototype.chatMsg = function (msg) {
    $('#myCamroomchatcontainer').append('<div class="chatmsg_'+msg.level+'"><span class="chatmsg_username">' + msg.user + ' : </span>' + msg.msg + '</div>');
}

CRoom.prototype.sendChat = function (msg) {
    this.socket.emit('chatMsg', msg);
}


$('#myCam_close').live('click', function (e) {
        e.preventDefault();
        croom.closeRoom();
});


function videoCapture(data) {
	$('#myCamTabLink').addClass('active').removeClass('hide');
	$('#camsTabLink, #viewingTabLink').removeClass('active');
	
	$('.tab-pane').removeClass('active');
	$('#tabMyCam').addClass('active');
	
    croom = new CRoom(data.room, data.roomName);
    croom.videoCapture(data);
    croom.resetChat();
}



$('#myCamroomchatform').live('submit', function (e) {
    e.preventDefault();
    var v = $('#myCamroomchatinput').val();
    if (v != "" && croom !==null) {
        croom.sendChat(v);
    }
    $('#myCamroomchatinput').val('');
});