//Declaració de variables
/***********************************************************************************************************/
var tools = require('./lib/globals.js');
var Users = require('./lib/master/user.js').Users;
var Servers = require('./lib/master/server.js').Servers;

var express = require('express'),
    app = express(),
    fs = require('fs'),
    httpsOptions = {
        key: fs.readFileSync('./ssl/master/master_key.pem'),
        cert: fs.readFileSync('./ssl/master/master_cert.crt'),
        ca: fs.readFileSync('./ssl/ca/ca_cert.crt'),
        requestCert: true,
        rejectUnauthorized: false,
        passphrase: "master"
    },
    dataProxyServer = require('https').createServer(httpsOptions, app),
	broadcasterServer = require('https').createServer(httpsOptions, app),
	videoProxyServer = require('https').createServer(httpsOptions, app),
    serviceServer = require('http').createServer(app),
    dataProxyIO = require('socket.io').listen(dataProxyServer),
    broadcasterIO = require('socket.io').listen(broadcasterServer),
    videoProxyIO = require('socket.io').listen(videoProxyServer),
    serviceIO = require('socket.io').listen(serviceServer),
    socketManager = require('socket.io-client');

/* variables globals del server */
var servers = new Servers();
var rooms = new Array();
var nextRoom = 1;
var users = new Users();
var rouleteUsers = new Array();

//Inicialització dels ports d'escolta 
/***********************************************************************************************************/
videoProxyServer.listen(MASTER_VIDEO_PROXY_PORT, function(){
    console.log('Master: escoltant al port ' + MASTER_VIDEO_PROXY_PORT + ' per conexions de videoProxy (amb ssl) :)');
});

dataProxyServer.listen(MASTER_DATA_PROXY_PORT, function(){
    console.log('Master: escoltant al port ' + MASTER_DATA_PROXY_PORT + ' per conexions de dataProxy (amb ssl) :)');
});

broadcasterServer.listen(MASTER_BROADCAST_PORT, function(){
    console.log('Master: escoltant al port ' + MASTER_BROADCAST_PORT + ' per conexions de broadcaster (amb ssl) :)');
});

serviceServer.listen(MASTER_SERVICE_PORT, function(){
    console.log('Master: escoltant al port ' + MASTER_SERVICE_PORT + ' per conexions de webservices :)');
});

videoProxyIO.configure(function(){
    videoProxyIO.disable('log');
})

dataProxyIO.configure(function(){
    dataProxyIO.disable('log');
})

broadcasterIO.configure(function(){
    broadcasterIO.disable('log');
})

serviceIO.configure(function(){
    serviceIO.disable('log');
})
/***********************************************************************************************************/

//Intervalo de refresco de salas + limpieza (2 minutos)
var refreshInterval = setInterval(function(){refreshRooms();}, 120000);


//Gestió dels missatges de video proxies
/***********************************************************************************************************/
videoProxyIO.sockets.on('connection', function(socket){
    var address = socket.handshake.address;
    var server_url = address.address + ":" + address.port
	tools.print_hash('videoProxy - connection:', address);
    /* Code TO-DO here */

    socket.on('addServer', function(data){
    	tools.print_hash('videoProxy - addServer. Data: ', data);
    	var id = servers.nextID();
        socket.serverID = id;
        servers.addVideoProxy(id,data.ip+':'+data.port, socket);  // faig hashing per port de proxy, agafo com a id el port del proxy de video (tot corre en localhost...)
        socket.emit("addServer-ack", {id: id});
        tools.print_hash('videoProxy - addServer. Afegit: ',servers.getVideoProxies());
    })

    socket.on('newBroadcast-ack', function(data){
        tools.print_hash('videoProxy - newBroadcast-ack. Data: ', data);
        servers.BroadcastEmit(data.broadcast.id, 'newProxy', {user: data.user, server: data.server, videoProxy: socket.serverID, room: data.room, password:data.password, type: data.type});
    })

    socket.on('disconnect', function(){
        console.log('videoProxy - disconnect: ' + socket.serverID);
        servers.delVideoProxy(socket.serverID);   //eliminem proxy del hash
        tools.print_hash('videoProxy - disconnect: ', servers.getVideoProxies());
    })
})
/***********************************************************************************************************/




//Gestió dels missatges de data proxies
/***********************************************************************************************************/
dataProxyIO.sockets.on('connection', function (socket) {
    var address = socket.handshake.address;
    var server_url = address.address + ":" + address.port
    tools.print_hash('dataProxy - connection:', address);

    socket.on('addServer', function (data) {
        tools.print_hash('dataProxy - addServer. Data: ', data);
        var id = servers.nextID();
        socket.serverID = id;
        servers.addDataProxy(id, data.ip + ':' + data.port, socket);  // faig hashing per port de proxy, agafo com a id el port del proxy de video (tot corre en localhost...)
        socket.emit("addServer-ack", { id: id });
        tools.print_hash('dataProxy - addServer. Afegit: ', servers.getDataProxies());
        refreshRooms();
		
		socket.on('disconnect', function () {
			console.log('dataProxy - disconnect: ' + socket.serverID);
			servers.delDataProxy(socket.serverID);   //eliminem proxy de dades del hash
			tools.print_hash('dataProxy - disconnect: ', servers.getDataProxies);
		});

		// ens demanen una nova sala
		socket.on('newRoom', function (data) {
			tools.print_hash('dataProxy - newRoom. Data: ', data)
			if (users.getUsername(data.user) !== null) {
				roomID = nextRoom++;                                            /************************************************Modificar*****/
				//l'afegim a l'estructura de dades
				var d = new Date();
				
				rooms[roomID] = { 
					id: roomID,
					user:data.user,
					creator: users.getUsername(data.user),
					broadcast: getBroadcast({}),
					state: "creating",
					frame: null,
					frames: data.options.frames,
					roomName: data.options.roomName,
					allowUnregistered: data.options.allowUnregistered,
					tokens: data.options.tokens,
					payp2p: data.options.payp2p,
					gen: data.options.gen,
					password: data.options.password,
					type: data.options.type,
					timer:d.getTime()
				};
				tools.print_hash('dataProxy - newRoom - addRoom:', rooms)
				//evisem al broadcaster que crei la nova sala
				//TODO treure el server -- l'hem d'agafar de l'id de sessio!
				servers.BroadcastEmit(rooms[roomID].broadcast, "newRoom", { user: data.user, server: socket.serverID, frames: data.options.frames, room: roomID, roomName: data.options.roomName, creator:rooms[roomID].creator });
			} else {
				socket.emit('newRoom-nack', { user: data.user, msg: "User not loged" });
			}
		})

		socket.on('joinRoom', function (data) {
			tools.print_hash('dataProxy - joinRoom. Data:', data);
			if (typeof rooms[data.room] !== 'undefined') {
				var room = rooms[data.room];
				if (room.state == "created") {
					var username = users.getUsername(data.user);
					if(!room.allowUnregistered && username===null){
						socket.emit('joinRoom-nack', { user: data.user, msg: "NeedRegisteredUser" });
					} else {
						if( ((room.type === 'private' || room.type === 'private_token')) && (room.password != data.password)) {
							socket.emit('joinRoom-nack', { user: data.user, msg: "BadPassword" });
						} else {
							var videoProxy = getBestVideoProxy(data.room);    //retorna key del video proxy
							var broadcast = servers.getBroadcast(rooms[data.room].broadcast);
							if (typeof broadcast !== null) {
								if( ((room.type === 'public_token' || room.type === 'private_token')) && (room.password == data.password)) {
                                    data.broadcaster = room.user;
                                    console.log(data);
									users.joinTokenCheck(username, room.creator, socket, data, room.tokens, servers, videoProxy, broadcast);
								} else {
									servers.VideoProxyEmit(videoProxy, 'newBroadcast', { broadcast: { id: broadcast.id, url: broadcast.purl }, room: data.room, user: data.user, username: username, server: socket.serverID, type: rooms[data.room].type });   //s'informa al proxy que un client vol veure la retransmissió
								}
							} else {
								socket.emit('joinRoom-nack', { user: data.user, msg: "Internal error" });
							}
							
						}
					}
				} else {
					socket.emit('joinRoom-nack', { user: data.user, msg: "Room not transmitting" });
				}
			} else {
				socket.emit('joinRoom-nack', { user: data.user, msg: "Room not exists" });
			}
		});

		//Login
		socket.on('login', function (data) {
			console.log('dataProxy - login user: ' + data.user);
			users.login(socket, data.user, socket.serverID, data.username, data.password);
		});

		//Signin
		socket.on('signin', function (data) {
			console.log('dataProxy - signin user: ' + data.user);
			users.signin(socket, data.user, socket.serverID, data.username, data.password);
		});

		//Logout
		socket.on('logout', function (data) {
			console.log('dataProxy - logout user: ' + data.user);
			users.logout(data.user, servers);
		});
		
		//Follow
		socket.on('follow', function (data) {
			console.log('dataProxy - follow user: ', data);
			var username = users.getUsername(data.user);
			if (username !== null) {
				users.follow(socket, data.user, username, data.follow);
			} else {
				socket.emit('follow-nack', {user:user, msg:"User not logged in"});
			}
		});

		//unFollow
		socket.on('unfollow', function (data) {
			console.log('dataProxy - unfollow user: ', data);
			var username = users.getUsername(data.user);
			if (username !== null) {
				users.unfollow(socket, data.user, username, data.follow);
			} else {
				socket.emit('unfollow-nack', {user:user, msg:"User not logged in"});
			}
		});

        socket.on('incTokens', function (data){
            console.log('dataProxy - incTokens: ', data);
            var username = users.getUsername(data.user);
            if(username !== null){
                users.incTokens(socket, data.user, username, data.tokens);
            } else{
                socket.emit('addTokens-nack', {user: user, msg: "User not logged in"});
            }
        });

        socket.on('sendTokens', function (data){
            console.log('dataProxy - sendTokens: ', data);
            var username = users.getUsername(data.user);
            if(username !== null){
				socketto = users.getSocket(data.userto);
                users.giveTokens(socket, data.user, username, socketto, data.userto, data.usernameto, data.tokens);
            } else{
                socket.emit('sendTokens-nack', {user: data.user, msg: "User not logged in"});
            }
        });
		
		
		socket.on('p2pAsk', function (data) {
			console.log('dataProxy - p2pAsk: ', data);
			
			if (users.inP2P(data.user) == null){
				var room = rooms[data.room];
				if (room.state == "created") {
					if (users.inP2P(room.user) == null){
						var username = users.getUsername(data.user);
						if(room.payp2p > 0) {
							if(username != null) {
								users.setP2P(data.user, room.user,room.payp2p);
								users.setP2P(room.user, data.user,0);
								users.checkP2P(data.user, data.username, room.payp2p, room.user);
							} else {
								socket.emit('p2pAsk-nack', {user:data.user, msg:'Register needed'});
							}
						} else {
							if(username == null) {
								username = 'Guest';
							}
							users.setP2P(data.user, room.user,0);
							users.setP2P(room.user, data.user,0);
							users.emit(room.user,'p2pAsk', {user:room.user, p2pwith:data.user, username:username});
						}
					} else {
						socket.emit('p2pAsk-nack', {user:data.user, msg:'User already on P2P'});
					}
				} else {
					socket.emit('p2pAsk-nack', {user:data.user, msg:'Room closed'});
				}
			} else {
				socket.emit('p2pAsk-nack', {user:data.user, msg:'Already on P2P'});
			}
		});
		
		socket.on('p2pAsk-nack', function (data) {
			console.log('dataProxy - p2pAsk-nack: ', data);
			if (users.inP2P(data.user) == data.p2pwith){
				users.setP2P(data.user, null,0);
			}
			if (users.inP2P(data.p2pwith) == data.user){
				users.setP2P(data.p2pwith, null,0);
				users.emit(data.p2pwith,'p2pAsk-nack', {user:data.p2pwith, msg:'User refused'});
			}
		});
		
		socket.on('p2pClose', function (data) {
			console.log('dataProxy - p2pClose: ', data);
			var p2pwith = users.inP2P(data.user);
			if (users.inP2P(p2pwith) == data.user){
				users.setP2P(p2pwith, null,0);
				users.emit(p2pwith,'p2pClose', {user:p2pwith, p2pwith:data.user});
				console.log('dataProxy - p2pClose to: ', p2pwith);
			} else{
				console.log('What appened?', p2pwith);
			}
			users.setP2P(data.user, null,0);
			
		});
		
		socket.on('p2pAsk-ack', function (data) {
			console.log('dataProxy - p2pAsk-ack: ', data);
			if (users.inP2P(data.user) == data.p2pwith){
				if(users.tokensP2P(data.p2pwith)>0){
					users.payP2P(data.p2pwith);
				}
				users.emit(data.p2pwith,'p2pAsk-ack', {user:data.p2pwith,p2pwith:data.user, username:users.getUsername(data.user)});
			} else {
				socket.emit('p2pAsk-nack', {user:data.user, msg:'What appened?'});
			}
		});
		
		
		//p2p
		socket.on('p2p-offer', function (data) {
			console.log('dataProxy - p2p-offer: ' + data.user);
			var p2pwith = users.inP2P(data.user);
			if (p2pwith != null){
				console.log('dataProxy - p2p-offer to: ' + p2pwith);
				users.emit(p2pwith,"p2p-offer", {user: p2pwith, data: data.data}); 
			}
		});
		
		socket.on('p2p-response', function (data) {
			console.log('dataProxy - p2p-response: ' + data.user);
			var p2pwith = users.inP2P(data.user);
			if (p2pwith != null){
				console.log('dataProxy - p2p-response to: ' + p2pwith);
				users.emit(p2pwith,"p2p-response", {user: p2pwith, data: data.data}); 
			}
		});
		 socket.on('p2p-candidate', function (data) {
			console.log('dataProxy - p2p-candidate: ' + data.user);
			var p2pwith = users.inP2P(data.user);
			if (p2pwith != null){
				console.log('dataProxy - p2p-candidate to: ' + p2pwith);
				users.emit(p2pwith,"p2p-candidate", {user: p2pwith, data: data.data}); 
			}
		});
    });

});




//Gestió de missatges de bradcasts
/***********************************************************************************************************/
broadcasterIO.sockets.on('connection', function (socket) {
    var address = socket.handshake.address;
    var server_url = address.address + ":" + address.port
    tools.print_hash('broadcast - connection:', address);


    socket.on('addServer', function (data) {
        tools.print_hash('broadcast - addServer. Data: ', data);
        var id = servers.nextID();
        socket.serverID = id;
        servers.addBroadcast(id, data.ip + ':' + data.port, data.ip + ':' + data.proxyport, socket);  // faig hashing per port de proxy, agafo com a id el port del proxy de video (tot corre en localhost...)
        socket.emit("addServer-ack", { id: id });
        tools.print_hash('broadcast - addServer. Afegit: ', servers.getBroadcasts());

        socket.on('newRoom-ack', function (data) {
            tools.print_hash('broadcast - newRoom-ack. Data:', data);
            //comprobem que la room existeixi
            if (rooms[data.room]) {
            	room=rooms[data.room];
                //comprobem que la room sigui correcte
                if (room.state == "creating") {
                    room.state = "wait";
                    servers.DataProxyEmit(data.server, "newRoom-ack", { user: data.user, room: room.id, roomName: room.roomName, frames: room.frames, password: data.password, url: servers.getBroadcast(rooms[room.id].broadcast).url });
                } else {
                    console.log("ERROR: la room no estar en l'estat correcte");
                    servers.DataProxyEmit(data.server, "newRoom-nack", { user: data.user, msg: "Internal error" });
                }
            } else {
                console.log("ERROR: roomID does not exist")
                servers.DataProxyEmit(data.server, "newRoom-nack", { user: data.user, msg: "Internal error" });
            }
        });

        socket.on('RoomFrame', function (data) {
            console.log('broadcast - Frame room. '+ data.room);
            //comprobem que la room existeixi
            if (typeof rooms[data.room] !== "undefined") {
                if (rooms[data.room].broadcast == socket.serverID) {
                    if (rooms[data.room].state == 'wait') {
                        rooms[data.room].state = "created";
                    }
					var d = new Date();
					rooms[data.room].timer = d.getTime();
                    rooms[data.room].frame = data.frame;
                    updateRoom(data.room);
                }
            }
        });

        socket.on('RoomPaused', function (data) {
            console.log('broadcast - Paused room. '+ data.room);
            //comprobem que la room existeixi
            if (typeof rooms[data.room] !== "undefined") {
                if (rooms[data.room].broadcast == socket.serverID) {
					var d = new Date();
					rooms[data.room].timer = d.getTime();
                }
            }
        });

        socket.on('RoomClose', function (data) {
            console.log('broadcast - close room '+ data.room);
            //comprobem que la room existeixi
            if (typeof rooms[data.room] !== "undefined") {
                if (rooms[data.room].broadcast == socket.serverID) {
                    rooms[data.room].state = "closed";
                    updateRoom(data.room);
                }
            }
        });

        socket.on('newRoom-nack', function (data) {
            servers.DataProxyEmit(data.server, "newRoom-nack", { user: data.user, msg: "Internal error" });
        })


        socket.on('newProxy-ack', function (data) {
            tools.print_hash('broadcast - newProxy-ack. Data: ', data);
            if (typeof rooms[data.room] !== "undefined") {
				var room = rooms[data.room];
				servers.DataProxyEmit(data.server, 'joinRoom-ack', { user: data.user, password: data.password, room: data.room, roomName: room.roomName, creator: room.creator, userRoom: room.user, payp2p: room.payp2p, url: servers.getVideoProxy(data.videoProxy).url, type: data.type });
			}
        });

        socket.on('disconnect', function () {
            console.log('broadcast - disconnect: ' + socket.serverID);
            servers.delBroadcast(socket.serverID);   //eliminem proxy del hash
            tools.print_hash('broadcast - disconnect: ', servers.getBroadcasts());
        });
    });


})
/***********************************************************************************************************/

//Gestió de missatges de bradcasts
/***********************************************************************************************************/
serviceIO.sockets.on('connection', function (socket) {
	console.log('service server connected');
});



//Funcions auxiliars
/***********************************************************************************************************/
function getBestVideoProxy(room){
    for(var videoProxy in servers.getVideoProxies()){
		return videoProxy;
    }
}

function getBroadcast(data){
    for(var broadcast in servers.getBroadcasts()){
		return broadcast;
    }
}



function updateRoom(room){
    var roomU = {
			id:rooms[room].id, 
			roomName:rooms[room].roomName, 
			creator:rooms[room].creator, 
			gen:rooms[room].gen, 
			state:rooms[room].state, 
			frame:rooms[room].frame, 
			type: rooms[room].type, 
			tokens: rooms[room].tokens,
			allowUnregistered : rooms[room].allowUnregistered
	};
    dataProxyIO.sockets.emit('update', roomU);
	serviceIO.sockets.emit('update', roomU);
}

function refreshRooms(){
	var d = new Date();
	//Delete if opened/closed (5min)
	var cto = d.getTime()-300000;
	//else (2min)
	var ctw = d.getTime()-120000;
	
    var roomlist = new Array();
    var deleteList = new Array();
    var i = 0;
    for(room in rooms){
		var r = rooms[room];
		if(r.state =='created' || r.state=='closed') {
			if(r.timer < cto){
				deleteList.push(room);
			} else {
				roomlist[i++] = {
					id:r.id, 
					roomName:r.roomName, 
					creator:r.creator, 
					gen:r.gen, 
					state:r.state, 
					frame:r.frame, 
					type: r.type, 
					tokens: r.tokens,
					allowUnregistered : r.allowUnregistered
				};
			}
		} else {
			if(r.timer < ctw){
				deleteList.push(room);
			}
		}
    }
    dataProxyIO.sockets.emit('list', roomlist);
    serviceIO.sockets.emit('list', roomlist);
	
	while(deleteList.length > 0) {
		room = deleteList.pop();
		delete rooms[room];
	}
}