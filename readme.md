#Projecte de Xarxes de Computadors 2012/13 q1

##La puesta en marcha del sistema necesita seguir varios pasos.

* Primero hemos de descargar el código fuente de los servidores y situarlo en el directorio elegido, al que llamaremos APPDIR

* Descargamos e instalamos Node.js <http://nodejs.org/> si no lo tenemos instalado previamente en el sistema.

* A continuación abrimos la consola de comandos y vamos al directorio APPDIR.

* Ejecutamos npm install. Esto nos descargará y compilara las dependencias. Es posible que nos pida descargar software adicional según la plataforma y software instalado.

* A continuación, si vamos a ejecutar el servidor master en la maquina, iniciamos el servidor MongoDB <http://www.mongodb.org/>. Por motivos de seguridad es buena idea configurar el servidor MongoDB para aceptar acceso únicamente de la ip loopback.

* Ya que la aplicación aún no está preparada para introducida para modificar parámetros en el momento del inicio, abrimos el archivo APPDIR/lib/globals.js.

* Modificamos la variable host, en la línea 5, por la ip de nuestra máquina o su url.

* Si el servidor master está en una máquina diferente, modificamos la variable MASTER_DN, en la linea 19, por la dirección de la máquina donde se encuentra master.

##Si queremos iniciar los servidores con los puertos por defecto (recomendado para facilitar la configuración de firewall si tenemos máximo un servidor de cada tipo por máquina) guardamos el fichero tal cual, en caso contrario modificamos:

* DATA_PROXY_PORT (linea 8) :             Puerto de escucha servidor dataProxy

* BROADCAST_START_RANGE (linea 22) :      Puerto de escucha servidor broadcaster

* BROADCAST_PROXY_START_RANGE (linea 23): Puerto de escucha servidor broadcaster 

* VIDEO_PROXY_START_RANGE (linea 29) :    Puerto de escucha servidor videoProxy

##Finalmente iniciamos los servidores con el comando node SERVER.js , donde SERVER es el tipo de servidor. Por seguridad, se ha de iniciar el servidor master antes que los otros servidores.