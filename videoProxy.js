//Declaració de variables
/***********************************************************************************************************/
var tools = require('./lib/globals.js'),
    Room = require('./lib/videoProxy/room.js'),
    Generator = require('./lib/videoProxy/passgenerator.js'),
    Broadcast = require('./lib/videoProxy/broadcast.js'),
    express = require('express'),
    app = express(),
    fs = require('fs'),
    PORT = process.env.PORT || VIDEO_PROXY_START_RANGE,
    httpsOptions = {
        key: fs.readFileSync('./ssl/videoProxy/videoProxy_key.pem'),
        cert: fs.readFileSync('./ssl/videoProxy/videoProxy_cert.crt'),
        ca: fs.readFileSync('./ssl/ca/ca_cert.crt'),
        requestCert: true,
        rejectUnauthorized: false,
        passphrase: "videoProxy"
    },
    httpsServer = require('https').createServer(httpsOptions,app),
    io = require('socket.io').listen(httpsServer),
    masterSocket = require('socket.io-client').connect('https://' + MASTER_DN +':'+ VIDEO_PROXY_MASTER_PORT),
    // variables globals del video proxy
    rooms = new Array(),
    serverID = null,
	nextGuest = 1,
    passGenerator = new Generator(1234);    
/***********************************************************************************************************/




//Inicialització del servidor
/***********************************************************************************************************/
httpsServer.listen(PORT, function(){
    console.log('Listening on video proxy server on port ' + PORT + 'with ssl :) ');
})

io.configure(function() {
    io.disable('log');
})

tools.getNetworkIP(function (error, ip) {
    if(!error){
        if(DEBUG) masterSocket.emit('addServer', {ip: 'https://' + VIDEO_PROXY_DN, port: PORT}); //immediatament registrem proxy de video al master      
        else masterSocket.emit('addServer', {ip: ip[0], port: PORT}); //immediatament registrem proxy de video al master      
    }
    else{
        console.log("Error iniciant servidor de broadcast...");
    }
}, false);
/***********************************************************************************************************/




//Gestió dels missatges amb els clients connectats al proxy
/***********************************************************************************************************/
io.sockets.on('connection', function (socket) {
    console.log('Algun client s\'ha connectat al proxy de video...');


    socket.on('ini', function (data) {
        //tools.print_hash('client - ini: Data', data);
        
    console.log(data);
        if ((typeof data.room === 'undefined') || (typeof data.user === 'undefined') || (typeof data.password === 'undefined') || (typeof rooms[data.room] === 'undefined')) {
            socket.emit('nack');
			console.log('nack');
        } else {
			console.log(rooms[data.room]);
            if (typeof rooms[data.room].futureUsers[data.user+':'+data.password] !== 'undefined') {
                if (rooms[data.room].broadcast.socket == null) {
                    rooms[data.room].broadcast.socket = require('socket.io-client').connect(rooms[data.room].broadcast.url, { 'force new connection': true });
                    rooms[data.room].broadcast.socket.emit('getFrames', { room: data.room, videoProxy: serverID });
                    rooms[data.room].broadcast.socket.room = data.room;
                    rooms[data.room].broadcast.socket.on('newFrame', function (data) {
                        io.sockets. in (this.room).emit('newFrame', data);
                    });
                    rooms[data.room].broadcast.socket.on('paused', function () {
                        io.sockets. in (this.room).emit('paused');
                    });
                    rooms[data.room].broadcast.socket.on('RoomClose', function (data) {
                        io.sockets. in (this.room).emit('RoomClose', data);
                    });
                    rooms[data.room].broadcast.socket.on('chatMsg', function (data) {
                        io.sockets. in (this.room).emit('chatMsg', data);
                    });
                }
                socket.room = data.room;
                socket.user = data.user;
				var username = rooms[data.room].futureUsers[data.user+':'+data.password];
				if(username === null) {
					socket.level = 'guest';
					socket.username = 'guest'+nextGuest;
					'guest'+nextGuest++;
				} else {
					socket.level = 'user';
					socket.username = username;
				}
					
                socket.join(data.room);
                rooms[data.room].users[data.user] = socket;
                delete rooms[data.room].futureUsers[data.user+':'+data.password];
                socket.emit('ack');

                socket.on('chatMsg', function (data) {
                    if (rooms[socket.room].broadcast.socket !== null) {
                        rooms[socket.room].broadcast.socket.emit('chatMsg', { username: socket.username, level: socket.level, msg: data });
                    }
                });
            } else {
                 socket.emit('nack');
				console.log('nack - nouser '+data.user+':'+data.password);
            }
        }



    });
});
/***********************************************************************************************************/




//Gestió dels missatges amb el master
/***********************************************************************************************************/
masterSocket.on('connect', function(){
	console.log('Proxy de video obre socket automàticament amb el master...');
})

masterSocket.on('disconnect', function () {
	process.exit(1);
});

masterSocket.on('addServer-ack', function(data){
	serverID = data.id;
	console.log("master - addServer-ack. serverID: " + serverID);
});

masterSocket.on('newBroadcast', function (data) {
    tools.print_hash('master - newBroadcast: Data', data);
    if (typeof rooms[data.room] === 'undefined') {
        rooms[data.room] = new Room(data.room, new Broadcast(data.broadcast.id, data.broadcast.url), data.type);
    }
    password = passGenerator.nextPass();
    rooms[data.room].addFutureUser(data.user, password, data.username);
    tools.print_hash('master - newBroadcast: Rooms 2', rooms);
    masterSocket.emit('newBroadcast-ack', { user: data.user,server: data.server, password: password, broadcast: data.broadcast, room: data.room, type: data.type }); //envio l'ack al master pel socket que toca
});
/***********************************************************************************************************/