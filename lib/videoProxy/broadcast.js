var Broadcast = function(id, url){
	this.id = id;
	this.url = url;
	this.socket = null;
}

module.exports = Broadcast;
