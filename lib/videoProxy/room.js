var Room = function(id, broadcast, user, type){
	this.id = id;
	this.broadcast = broadcast;
	this.type = type;
	this.users = {};
	this.futureUsers = {};
}

Room.prototype.addFutureUser = function(user, password, username){
	this.futureUsers[user+':'+password] = username;
}

Room.prototype.broadcastSocket = function(broadcasts){
	return broadcasts[this.broadcast].socket;
}

module.exports = Room;

