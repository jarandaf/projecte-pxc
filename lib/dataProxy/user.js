var Users = function () {
    this.sessions = new Array();
    this.users = new Array();
    this.id = Math.ceil(Math.random() * 5000);
    this.server = 0;
}

Users.prototype.setServerId = function(server){
	this.server = server;
}

Users.prototype.enters = function(socket){
	var uid = this.id++;
	var user = this.server+':'+uid;
	this.sessions[user] = {
		id : uid,
		socket: socket,
		username:null
	};
	
	return uid;
}

Users.prototype.emit = function(user, event, data){
    if (typeof this.sessions[user] === "undefined")
        return;
	this.sessions[user].socket.emit(event, data);
}



Users.prototype.getUsername = function(uid){
    var user = this.server+':'+uid;
	if(typeof this.sessions[user] === "undefined")
		return null;
	return this.sessions[user].username;
}

Users.prototype.login = function(user, username){
	console.log(user);
	this.sessions[user].username = username;
	if(typeof this.users[username] === "undefined") {
		this.users[username] = new Array();
	}
	this.users[username][user] = true;
}

Users.prototype.logout = function(uid){
	var user = this.server+':'+uid;
	if(typeof this.sessions[user] === "undefined")
		return;
		
	var username = this.sessions[user].username;
	this.sessions[user].username = null;
	delete this.users[username][user];
	if(this.users[username].length<=0){
		delete this.users[username];
	}
}

Users.prototype.exits = function (uid) {
    var user = this.server + ':' + uid;
    if (typeof this.sessions[user] === "undefined")
        return;
        
    console.log('user exits', uid);
    console.log('uid', uid);
    var username = this.sessions[user].username;
    console.log('uname', username);
    delete this.sessions[user];
    if ((username !== null)&&(typeof this.users[username] != 'undefined')) {
        delete this.users[username][user];
        if (this.users[username].length <= 0) {
            delete this.users[username];
        }
    }
}

module.exports.Users = Users;
