//Definició d'estructures de dades

var Servers = function(){
	this.broadcasts = {};
	this.videoProxies = {};
	this.dataProxies = {};
	this.all = {};
	this.id = 1;
}

var dataProxyServer = function(id, url, socket){
	//A ampliar...
	this.id = id;
	this.url = url;
	this.socket = socket;
}

var broadcastServer = function(id, url, purl, socket){
	//A ampliar...
	this.id = id;
	this.url = url;
	this.purl = purl;
	this.socket = socket;
}

var videoProxyServer = function(id, url, socket){
	//A ampliar...
	this.id = id;
	this.url = url;
	this.socket = socket;
}

// Prototips de funcions

Servers.prototype.addDataProxy = function(id, url, socket){
	this.all[id] = new dataProxyServer(id,url,socket);
	this.dataProxies[id] = id;
}

Servers.prototype.addVideoProxy = function(id, url, socket){
	this.all[id] = new videoProxyServer(id,url,socket);
	this.videoProxies[id] = id;
}

Servers.prototype.addBroadcast = function(id, url, purl, socket){
	this.all[id] = new broadcastServer(id,url,purl,socket);
	this.broadcasts[id] = id;
}

Servers.prototype.getDataProxy = function(id){
	if(this.dataProxies[id]){
		return this.all[id];
	}
	return null;
}

Servers.prototype.getVideoProxy = function(id){
	if(this.videoProxies[id]){
		return this.all[id];
	}
	return null;
}

Servers.prototype.getBroadcast = function(id){
	if(this.broadcasts[id]){
		return this.all[id];
	}
	return null;
}

Servers.prototype.delDataProxy = function(id){
	if(this.dataProxies[id]){
		delete this.dataProxies[id];
		delete this.all[id];
	}
}

Servers.prototype.delVideoProxy = function(id){
	if(this.videoProxies[id]){
		delete this.videoProxies[id];
		delete this.all[id];
	}
}

Servers.prototype.delBroadcast = function(id){
	if(this.broadcasts[id]){
		delete this.broadcasts[id];
		delete this.all[id];
	}
}

Servers.prototype.getVideoProxies = function(){
	var res = {};
	console.log("getVideoProxies: ");
	for (videoProxy in this.videoProxies){
		res[videoProxy] = this.all[videoProxy];
	}
	return res;
}

Servers.prototype.getBroadcasts = function(){
	var res = {};
	for (braodcast in this.broadcasts){
		res[braodcast] = this.all[braodcast];
	}
	return res;
}

Servers.prototype.getDataProxies = function(){
	var res = {};
	for (dataProxy in this.dataProxies){
		res[dataProxy] = this.all[dataProxy];
	}
	return res;
}

Servers.prototype.nextID = function(){
	var id = this.id++;
	return id;
}


Servers.prototype.DataProxyEmit = function (id, event, data) {
    if (this.dataProxies[id]) {
        this.all[id].socket.emit(event, data);
        return true;
    }
    return false;
}

Servers.prototype.BroadcastEmit = function (id, event, data) {
    if (this.broadcasts[id]) {
        this.all[id].socket.emit(event, data);
        return true;
    }
    return false;
}

Servers.prototype.VideoProxyEmit = function (id, event, data) {
    if (this.videoProxies[id]) {
        this.all[id].socket.emit(event, data);
        return true;
    }
    return false;
}

module.exports.Servers = Servers;