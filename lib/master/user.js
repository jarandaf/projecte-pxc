var mongoose = require('mongoose');

// User schema
var userschema = new mongoose.Schema({
	username: String,
	password: String,
	tokens: Number,
	follow: Array(String)
},{collection:'User'});

userschema.statics.findByName = function (username, cb) {
	this.find({ username: username }, cb);
}

userschema.statics.incTokens = function (socket, user, username, value) {
	this.update({username: username }, {$inc : { tokens: value }}, {upsert :true},function(err){
        if(err){
                console.log(err);
                socket.emit('incTokens-nack', {user: user, msg: "Internal ERROR"});
        }else{
                console.log("incTokens-ack");
                socket.emit('incTokens-ack', {user: user, tokens: value});
        }
	});
}

userschema.statics.decTokens = function (socket, user, username, value) {
	this.update({username: username }, {$inc : { tokens: -value }}, {upsert :true},function(err){
        if(err){
                console.log(err);
        }else{
                console.log("decTokens");
                socket.emit('decTokens-ack', {user: user, tokens: value});
        }
	});
}

userschema.statics.giveTokens = function (socket, user, from, socketto, userto, to, value) {
	var DB = this;
	DB.update({username: from }, {$inc : { tokens: -value }}, {upsert :true},function(err){
        if(err){
                console.log(err);
                socket.emit('sendTokens-nack', {user: user, msg: "Internal ERROR"});
        }else{
			DB.update({username: to }, {$inc : { tokens: value }}, {upsert :true},function(err){
				if(err){
					console.log(err);
					socket.emit('sendTokens-nack', {user: user, msg: "Internal ERROR"});
					DB.update({username: from }, {$inc : { tokens: value }}, {upsert :true},function(err){
						if(err){ console.log(err); }
					});
				} else {
						console.log("decTokens");
						socket.emit('sendTokens-ack', {user: user, tokens: value, to:to});
						if(socketto != null){
							socketto.emit('receivedTokens-ack', {user: userto, tokens: value, from:from});
						}
				}
			});
        }
	});
}

userschema.statics.follow = function (username, follows) {
	console.log({$push : { follow: follows }});
	this.update({username: username }, {$push : { follow: follows }}, {upsert :true},function(err){
        if(err){
                console.log(err);
        }else{
                console.log("Follow: successfully added");
        }
	});
}

userschema.statics.unfollow = function (username, follows) {
	this.update({username: username }, {$pull : { follow: follows }}, {upsert :true},function(err){
        if(err){
                console.log(err);
        }else{
                console.log("Unfollow: Successfully deleted");
        }
	});
}

mongoose.model('User', userschema);

// Connection
var conn = mongoose.createConnection('localhost', 'cam');



var Users = function(){
	this.sessions = new Array();
	this.users = new Array();
	console.log(this.sessions);
	console.log(this.users);
}



function hashPass(pass) {
	return pass;
}

Users.prototype.login = function(socket, user, serverID, username, password){
	var usersData = this;
	password = hashPass(password);
	
	var MUser = conn.model('User', userschema);
	MUser.findByName(username, function (err, users) {
		if((users.length>0) && (users[0].password == password)){
			usersData.sessions[user] = {
				username:username, 
				server:serverID,
				socket:socket,
				p2pwith: null,
				p2ptokens :0
			};
			if(typeof usersData.users[username] === "undefined") {
				usersData.users[username] = new Array();
			}
			usersData.users[username][user] = serverID;
			var follow;
			if(typeof users[0].follow != 'undefined') {
				follow = users[0].follow;
			} else {
				follow = new Array();
			}
			
			console.log(users[0]);
			socket.emit('login-ack', {user:user, username:username, follow:follow, tokens: users[0].tokens});
		} else {
			socket.emit('login-nack', {user:user});
		}
	});
}

Users.prototype.signin = function(socket, user, serverID, username, password){
	var usersData = this;
	password = hashPass(password);
	
	var MUser = conn.model('User', userschema);
	var us = new MUser({ 
		username: username,
		password:password,
		tokens: 5,
		follow: new Array()
	});
	MUser.findByName(username, function (err, users) {
		if (err){
			socket.emit('signin-nack', {user:user, msg:"Internal ERROR"});
		} else if(users.length>0) {
			socket.emit('signin-nack', {user:user, msg:"UserName already in our database"});
		} else {
			us.save(function (err) {
				if (err){
					socket.emit('signin-nack', {user:user, msg:"Internal ERROR"});
				} else {
					usersData.sessions[user] = {
						username:username, 
						server:serverID,
						socket:socket,
						p2pwith: null,
						p2ptokens :0
					};
					if(typeof usersData.users[username] === "undefined") {
						usersData.users[username] = new Array();
					}
					usersData.users[username][user] = serverID;
					socket.emit('signin-ack', {user:user, username:username});
				}
			});
		}
	});
}

Users.prototype.logout = function(user, servers){
	if(typeof this.sessions[user] === "undefined")
		return;
	var username = this.sessions[user];
	var p2pwith = this.sessions[user].p2pwith;
	
	delete this.sessions[user];
    if(!((typeof username === "undefined")||(typeof this.users[username] === "undefined")))
	{	
		delete this.users[username][user];
		if(this.users[username].length<=0){
			delete this.users[username];
		}
	}
    if(!((typeof p2pwith === "undefined")||(typeof this.sessions[p2pwith] === "undefined")))
	{	
		var usp2p = this.sessions[p2pwith];
		if(usp2p.p2pwith===user){
			usp2p.p2pwith = null;
			usp2p.socket.emit('closeP2P',{user:p2pwith, p2pwith:user} );
		}
	}
}


Users.prototype.getUsername = function (user) {
    if (typeof this.sessions[user] === "undefined")
        return null;
    return this.sessions[user].username;
}

Users.prototype.inP2P = function (user) {
    if (typeof this.sessions[user] === "undefined")
        return null;
    return this.sessions[user].p2pwith;
}
Users.prototype.setP2P = function (user, p2pwith, tokens) {
    if (typeof this.sessions[user] === "undefined")
        return null;
    this.sessions[user].p2pwith = p2pwith;
    this.sessions[user].p2ptokens = tokens;
}

Users.prototype.tokensP2P = function (user) {
    if (typeof this.sessions[user] === "undefined")
        return 0;
    return this.sessions[user].p2ptokens;
}


Users.prototype.emit = function (user, event, data) {
    if (typeof this.sessions[user] === "undefined")
        return false;
	this.sessions[user].socket.emit(event, data);
    return true;
}
Users.prototype.getSocket = function (user) {
    if (typeof this.sessions[user] === "undefined")
        return null;
	return this.sessions[user].socket;
}

Users.prototype.joinTokenCheck = function (usernamePays, usernameEarns, socket, data, tokens, servers, videoProxy, broadcast) {
	var MUser = conn.model('User', userschema);
	MUser.findByName(usernamePays, function (err, users) {
		if (err){
			socket.emit('joinRoom-nack', {user:data.user, msg:"Internal ERROR"});
		} else if(users.length>0){
			console.log(users);
			if(users[0].tokens >= tokens){
				MUser.decTokens(socket, data.user, usernamePays, tokens);
				servers.VideoProxyEmit(videoProxy, 'newBroadcast', { broadcast: { id: broadcast.id, url: broadcast.purl }, room: data.room, user: data.user, username: usernamePays, server: socket.serverID});
				MUser.findByName(usernameEarns, function (err, users){
					if(err){
						socket.emit('joinRoom-nack', {user: data.broadcaster, msg: "Internal ERROR"});
					} else if(users.length>0){
						console.log(users);
						MUser.incTokens(socket, data.broadcaster, usernameEarns, tokens);
					}
				});
			} else {
				socket.emit('joinRoom-nack', {user:data.user, msg:"Need more tokens"});
			}
		} else {
			socket.emit('joinRoom-nack', {user:data.user, msg:"User not exists ERROR"});
		}
	});
}

Users.prototype.giveTokens = function (socket, user, usernamePays, socketto, userto, usernameEarns, tokens) {
	var MUser = conn.model('User', userschema);
	MUser.findByName(usernamePays, function (err, users) {
		if (err){
			socket.emit('sendTokens-nack', {user:user, msg:"Internal ERROR"});
		} else if(users.length>0){
			console.log(users);
			console.log(tokens);
			if(users[0].tokens >= tokens){
				MUser.giveTokens(socket, user, usernamePays, socketto, userto, usernameEarns, tokens);
			} else {
				socket.emit('sendTokens-nack', {user:user, msg:"Need more tokens"});
			}
		} else {
			socket.emit('sendTokens-nack', {user:user, msg:"User not exists ERROR"});
		}
	});
}

Users.prototype.checkP2P = function (user, username, tokens, p2pwith) {
	var t = this;
	var user_data = typeof this.sessions[user];
	var p2pwith_data = typeof this.sessions[p2pwith];
	var MUser = conn.model('User', userschema);
	MUser.findByName(username, function (err, users) {
		if (err){
			t.setP2P(user, null,0);
			t.setP2P(p2pwith, null,0);
			user_data.socket.emit('p2pAsk-nack', {user:user, msg:'Internal Error'});
		} else if(users.length>0){
			console.log(users);
			if(users[0].tokens >= tokens){
				p2pwith_data.socket.emit('p2pAsk', {user:p2pwith, p2pwith:user, username:username});
			} else {
				t.setP2P(user, null,0);
				t.setP2P(p2pwith, null,0);
				user_data.socket.emit('p2pAsk-nack', {user:user, msg:'Not enough tokens'});
			}
		} else {
			t.setP2P(user, null,0);
			t.setP2P(p2pwith, null,0);
			user_data.socket.emit('p2pAsk-nack', {user:user, msg:'Internal Error'});
		}
	});
}




Users.prototype.follow = function (socket, user, username, follow) {
	var MUser = conn.model('User', userschema);
	MUser.findByName(username, function (err, users) {
		if (err){
			socket.emit('follow-nack', {user:user, msg:"INTERNAL ERROR"});
		} else if(users.length>0){
			MUser.follow(username, follow);
			console.log('follow-ack');
			socket.emit('follow-ack', {user:user, follow:follow});
		} else {
			socket.emit('follow-nack', {user:user, msg:"User to follow not exists"});
		}
	});
}

Users.prototype.unfollow = function (socket, user, username, follow) {
	var MUser = conn.model('User', userschema);
	MUser.unfollow(username, follow);
	console.log('unfollow-ack');
	socket.emit('unfollow-ack', {user:user, follow:follow});
}

Users.prototype.incTokens = function(socket, user, username, value){
	var MUser = conn.model('User', userschema);
	MUser.findByName(username, function (err, users){
		if(err){
			console.log('incTokens-nack');
			socket.emit('incTokens-nack', {user: user, msg: "INTERNAL ERROR"});
		} else if(users.length>0){
			MUser.incTokens(socket,user,username,value);
			console.log('incTokens-ack');
		} else {
			console.log('incTokens-nack');
			socket.emit('incTokens-nack', {user: user, msg: "User to increment tokens does not exist"});
		}
	});
}
Users.prototype.payP2P = function(user){
	if (typeof this.sessions[user] === "undefined")
        return ;
	var user_data = this.sessions[user];
	var MUser = conn.model('User', userschema);
	MUser.decTokens(user_data.socket, user, user_data.username, user_data.p2ptokens);
}

module.exports.Users = Users;
