var Room = function (id, name, user, creator, password) {
	//identificador de la sala
    this.id = id;
    this.name = name;
	
    // contraseña para emitir video
    this.password = password;
    // videoproxies que estan retransmitiendo la sala
    this.videoProxies = new Array();
    // videoproxies que van a conectarse para retransmitir una sala
    this.futureVideoProxies = new Array();
    // creador
    this.creator = {
        user: user,
        socket: null,
		username: creator
    };
    this.lastFrame = 0;
    this.lastPause = 0;
}

Room.prototype.addFutureVideoProxy = function(videoProxy){
	this.futureVideoProxies[videoProxy] = videoProxy;
}

module.exports = Room;

