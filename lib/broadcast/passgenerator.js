var Generator = function (semilla) {
    this.next = semilla;
}

Generator.prototype.nextPass = function () {
    return this.next++;
}

module.exports = Generator;
