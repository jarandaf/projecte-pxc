DEBUG = 1;

FRAMES_PER_CAPTURE = 1000;

var host = 'localhost';

//Data proxy
DATA_PROXY_PORT = 443;
DATA_PROXY_ACK = 1999;
DATA_PROXY_MASTER_PORT = 2001;
DATA_PROXY_DN = host;

//Master
MASTER_PORT = 2000;
MASTER_DATA_PROXY_PORT = 2001;
MASTER_BROADCAST_PORT = 2002;
MASTER_VIDEO_PROXY_PORT = 2003;
MASTER_SERVICE_PORT = 2004;
MASTER_DN = host;

//Broadcast
BROADCAST_START_RANGE = 3000;
BROADCAST_PROXY_START_RANGE = 5000;
BROADCAST_MASTER_PORT = 2002;
BROADCAST_DN = host;

//Video proxy
VIDEO_PROXY_START_RANGE = 4000;
VIDEO_PROXY_MASTER_PORT = 2003;
VIDEO_PROXY_DN = host;

// Array Remove - By John Resig (MIT Licensed)
Array.prototype.remove = function(from, to) {
  var rest = this.slice((to || from) + 1 || this.length);
  this.length = from < 0 ? this.length + from : from;
  return this.push.apply(this, rest);
};


module.exports = {
	print_hash: function(str, varHash){
		console.log(str + " [");
		for(property in varHash) {
			if (typeof(varHash[property]) == "object" && property != "socket"){
				console.log("\t" + property + " [");
				for(property2 in varHash[property]) {



					if (typeof(varHash[property][property2]) == "object" && property2 != "socket"){
						console.log("\t\t" + property2 + " [");
						for(property3 in varHash[property][property2]) {
							console.log("\t\t\t"+property3 + ': ' + varHash[property][property2][property3] + " - " + typeof(varHash[property][property2][property3]));
						}
						console.log("\t\t]");
							
					} else{
						console.log("\t\t"+property2 + ': ' + varHash[property][property2] + " - " + typeof(varHash[property][property2]));
					}

				}
				console.log("\t]");
					
			} else{
				console.log("\t"+property + ': ' + varHash[property] + " - " + typeof(varHash[property]));
			}
		}
		console.log("]");
	},
	getNetworkIP: function () {
		var ignoreRE = /^(127\.0\.0\.1|::1|fe80(:1)?::1(%.*)?)$/i;

		var exec = require('child_process').exec;
		var cached;
		var command;
		var filterRE;

		switch (process.platform) {
			case 'win32':
			//case 'win64': // TODO: test
			    command = 'ipconfig';
			    filterRE = /\bIP(v[46])?-?[^:\r\n]+:\s*([^\s]+)/g;
			    // TODO: find IPv6 RegEx
			    break;
			case 'darwin':
			    command = 'ifconfig';
			    filterRE = /\binet\s+([^\s]+)/g;
			    // filterRE = /\binet6\s+([^\s]+)/g; // IPv6
			    break;
			default:
			    command = 'ifconfig';
			    filterRE = /\binet\b[^:]+:\s*([^\s]+)/g;
			    // filterRE = /\binet6[^:]+:\s*([^\s]+)/g; // IPv6
			    break;
		}

		return function (callback, bypassCache) {
		    if (cached && !bypassCache) {
		        callback(null, cached);
		        return;
		    }
		    // system call
		    exec(command, function (error, stdout, sterr) {
		        cached = [];
		        var ip;
		        var matches = stdout.match(filterRE) || [];
		        //if (!error) {
		        for (var i = 0; i < matches.length; i++) {
		            ip = matches[i].replace(filterRE, '$1')
		            if (!ignoreRE.test(ip)) {
		                cached.push(ip);
		            }
		        }
		        //}
		        callback(error, cached);
		    });
		};
	}()
};
