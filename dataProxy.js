//Declaració de variables
/***********************************************************************************************************/

var tools = require('./lib/globals.js');
var Users = require('./lib/dataProxy/user.js').Users;

var express = require('express'),
	app = express(),
	fs = require('fs');

var rooms = new Array();
	
var serverID = 0;
var users = new Users();

/* A falta de definir las rutas para express... uso path relativo */
var httpsOptions = {
	key: fs.readFileSync('./ssl/dataProxy/dataProxy_key.pem'),
	cert: fs.readFileSync('./ssl/dataProxy/dataProxy_cert.crt'),
	ca: fs.readFileSync('./ssl/ca/ca_cert.crt'),
	requestCert: true,
	rejectUnauthorized: false,
	passphrase: "dataProxy"
};

/* PAYPAL credentials & options */

var credentials = {
	username: 'jordi_1354101376_biz_api1.gmail.com',
	password: '1354101397',
	signature: 'AK19JHjGEqJC5WXE4a8GYLvReliZADTO1zUdvMYVWgCWXgf6V1NwYWm7'
};

var options = {
	sandbox: true,
	version: '95.0'
}

var params1,params2; //variable "global" para el usuario (paypal)

var PayPalEC = require('paypal-ec');
var ec = new PayPalEC(credentials,options);

/* Prueba iniciando un servidor ssl directamente */
var httpsServer = require('https').createServer(httpsOptions,app),
	io = require('socket.io').listen(httpsServer),
	masterSocket = require('socket.io-client').connect('https://' + MASTER_DN +':'+ DATA_PROXY_MASTER_PORT);
/***********************************************************************************************************/

//Inicialització del servidor
/***********************************************************************************************************/
httpsServer.listen(DATA_PROXY_PORT, function(){
	console.log('Listening on dataProxy server on port ' + DATA_PROXY_PORT + '...');
})

app.configure(function(){
	app.set('view options', {
		layout: false
	});
	app.use(express.static(__dirname + '/static'));
})

io.configure(function() {
	io.disable('log');
	io.set('transports', ['websocket', 'xhr-polling']);
})

tools.getNetworkIP(function (error, ip) {
	if(!error){
		if(DEBUG) masterSocket.emit('addServer', {ip: 'https://' + DATA_PROXY_DN, port: DATA_PROXY_PORT}); //immediatament registrem proxy de video al master	  
		else masterSocket.emit('addServer', {ip: ip[0], port: DATA_PROXY_PORT}); //immediatament registrem proxy de video al master	  
	}
	else{
		console.log("Error iniciant servidor de dataProxy...");
	}
}, false);
/***********************************************************************************************************/



//Gestió dels missatges amb els clients connectats al servidor web
/***********************************************************************************************************/
io.sockets.on('connection', function (socket) {
	if (serverID <= 0) {
		socket.emit('error', { msg: "Server not connected" });
	} else {
	
	var d = new Date();
	var t = d.getTime();
	socket.connectedAt = t;
	
		var uid = users.enters(socket);
		socket.uid = uid;
		socket.emit('connected', { user: (serverID + ':' + uid), rooms: rooms });
		console.log('New client connected to dataProxy...', uid);

		//Create Room
		socket.on('newRoom', function (data) {
			console.log('Crear Sala');
			var username = users.getUsername(socket.uid);
			if (username === null) {
				socket.emit('newRoom-nack', {});
			} else {
				var allOk = true;
				var toCheck = new Array();
				/* Test datos */
				if((typeof data.roomName === 'undefined')||data.roomName === "") {
					allOk = false;
					toCheck.push('neededRoom');
				}
				if(typeof data.password === 'undefined') {
					data.password = '';
				}
				
				
				if((typeof data.tokens === 'undefined')||data.tokens === "") {
					data.tokens = 0;
				} else if(isNaN(parseInt(data.tokens)) || (data.tokens<0)) {
					allOk = false;
					toCheck.push('invalidTokens');
				}
				
				if((typeof data.payp2p === 'undefined')||data.payp2p === "") {
					data.payp2p = 0;
				} else if(isNaN(parseInt(data.payp2p)) || (data.payp2p<0)) {
					allOk = false;
					toCheck.push('invalidPayp2p');
				}
				
				if(typeof data.allowUnregistered == 'undefined' || data.allowUnregistered==false || data.tokens>0) {
					data.allowUnregistered = false;
				} else {
					data.allowUnregistered = true;
				}
				
				if((typeof data.gen === 'undefined')|| ((data.gen !== "male")&&(data.gen !== "female")&&(data.gen !== "couple"))) {
					allOk = false;
					toCheck.push('invalidGen');
				}
				
				switch(data.type) {
					case 'public':
						break;
					case 'public_token':
						if(data.tokens ==0) {
							data.type = 'public';
						}
						break;
					case 'private':
						if(data.password == "") {
							allOk = false;
							toCheck.push('passNeed');
						}
						break;
					case 'private_token':
						if(data.tokens ==0) {
							data.type = 'private';
						}
						if(data.password == "") {
							allOk = false;
							toCheck.push('passNeed');
						}
						break;
					default:
						allOk = false;
						toCheck.push('typeNotDefined');
				}
				
				if(allOk) {
					masterSocket.emit('newRoom', { user: serverID + ':' + socket.uid, options: data });
				} else {
					socket.emit('newRoom-check', toCheck);
				}
			}
		});

		socket.on('joinRoom', function (data) {
			console.log('Ver Sala ', data);
			masterSocket.emit('joinRoom', { user: serverID + ':' + socket.uid, room: data.room, password: data.password});
		});

		socket.on('disconnect', function () {
		
			var d = new Date();
			var t = d.getTime();
			console.log('active time', (t-socket.connectedAt));
			
			var username = users.getUsername(socket.uid);
			if (username !== null) {
				console.log('disconnect ', username);
				masterSocket.emit('logout', { user: serverID + ':' + socket.uid });
			}
			users.exits(socket.uid);
		});

		//LogIn
		socket.on('login', function (data) {
			console.log('login');
			var username = users.getUsername(socket.uid);
			if (username !== null) {
				socket.emit('login-ack', { username: data.username });
			} else if ((typeof data != 'undefined') && (typeof data.username != 'undefined') && (data.username != "") && (typeof data.password != 'undefined') && (data.password != "")) {
				masterSocket.emit('login', { user: serverID + ':' + socket.uid, username: data.username, password: data.password });
			} else {
				socket.emit('login-nack');
			}
		});

		//SignIn
		socket.on('signin', function (data) {
			console.log('signin');
			var username = users.getUsername(socket.uid);
			if (username !== null) {
				socket.emit('signin-nack', { msg: 'You are already loged' });
			} else if ((typeof data != 'undefined') && (typeof data.username != 'undefined') && (data.username != "") && (typeof data.password != 'undefined') && (data.password != "") && (typeof data.password2 != 'undefined') && (data.password2 != "")) {
				if (data.password != data.password2) {
					socket.emit('signin-nack', { msg: 'Passwords do not match' });
				} else {
					masterSocket.emit('signin', { user: serverID + ':' + socket.uid, username: data.username, password: data.password });
				}
			} else {
				socket.emit('signin-nack', { msg: 'Some data is missing' });
			}
		});

		//LogOut
		socket.on('logout', function (data) {
			var username = users.getUsername(socket.uid);
			if (username !== null) {
				console.log('logout ', username);
				masterSocket.emit('logout', { user: serverID + ':' + socket.uid });
			}
			users.logout(socket.uid);
			socket.emit('list', rooms);
		});

		//Follow
		socket.on('follow', function (follow) {
			var username = users.getUsername(socket.uid);
			if (username !== null) {
				if(typeof follow !== 'undefined' && follow !== null && follow != "") {
					masterSocket.emit('follow', { user: serverID + ':' + socket.uid , follow:follow});
				} else {
					socket.emit('follow-nack', {msg:"bad request"});
				}
			} else {
				socket.emit('follow-nack', {msg:"User not loged"});
			}
		});

		//unFollow
		socket.on('unfollow', function (follow) {
			var username = users.getUsername(socket.uid);
			if (username !== null) {
				if(typeof follow !== 'undefined' && follow !== null && follow != "") {
					masterSocket.emit('unfollow', { user: serverID + ':' + socket.uid , follow:follow});
				} else {
					socket.emit('follow-nack', {msg:"bad request"});
				}
			} else {
				socket.emit('follow-nack', {msg:"User not loged"});
			}
		});

		//paypal
		socket.on('doPayment', function (data){
			params1 = {
				returnUrl : 'https://localhost/success.html',
				cancelUrl : 'https://localhost/cancel.html',
				SOLUTIONTYPE				   : 'sole',
				PAYMENTREQUEST_0_AMT		   : data.amount/10, //a concretar según lo que el usuario desee -> 1 token = 10cts
				PAYMENTREQUEST_0_DESC		  : 'PXC - Tokens Sale \n Desired number of tokens: ' + data.amount,
				PAYMENTREQUEST_0_CURRENCYCODE  : 'EUR',
				PAYMENTREQUEST_0_PAYMENTACTION : 'Sale',
			};

			params2 = {
				user: data.user,	 //hacks para poder recuperar el usuario tras la redirección
				username: data.username
			}

			ec.set(params1, function (error, data){
				console.log('ERROR? ' + error);
				console.log('new token received...! ' + data.TOKEN);
				params1.TOKEN = data.TOKEN;
				ec.get_details(params1, function (error, data){
					console.log('ERROR? ' + error);
					console.log('data: ');
					console.log(data);
					params1 = data;
				});
				socket.emit('paypalRedirection', {paymentUrl: data.PAYMENTURL});
			});
		});

		socket.on('success', function (data){
			params1.PayerID = data.payerID;
			ec.do_payment(params1, function (error, data){
				console.log('ERROR? ' + error);
				console.log('params: ');
				console.log(params1);
				console.log('amount has been successfully charged');
				masterSocket.emit('incTokens', {user: params2.user, username: params2.username, tokens: params1.PAYMENTREQUEST_0_AMT*10});
			});
		});

		socket.on('sendTokens', function (data){
			var username = users.getUsername(socket.uid);
			if (username !== null) {
				var userto = data.userto;
				var usernameto = data.usernameto;
				var tokens = data.tokens;
				if((typeof userto != 'undefined') && (typeof usernameto != 'undefined') && (typeof tokens != 'undefined') && !isNaN(parseInt(tokens)) || (tokens>0)) {
					masterSocket.emit('sendTokens', {user: serverID+':'+socket.uid, userto:userto, usernameto:usernameto, tokens:tokens});
				} else {
					socket.emit('sendTokens-nack',{msg:'Bad request'});
				}
			} else {
				socket.emit('sendTokens-nack',{msg:'Not loged'});
			}
		});
	
	
		socket.on('p2pAsk', function(data){
			console.log('user - p2p', data);
			var username = users.getUsername(socket.uid);
			if (username !== null) {
				masterSocket.emit('p2pAsk', {user: serverID+':'+socket.uid, room:data.room});
			} else {
				socket.emit('p2pAsk-nack',{msg:'Not loged'});
			}
		});
		
		socket.on('p2pAsk-nack', function(data){
			console.log('user - roulete', data);
			masterSocket.emit('p2pAsk-nack', {user: serverID+':'+socket.uid, p2pwith:data.p2pwith});
		});
		
		socket.on('p2pAsk-ack', function(data){
			console.log('user - roulete', data);
			masterSocket.emit('p2pAsk-ack', {user: serverID+':'+socket.uid, p2pwith:data.p2pwith});
		});

		socket.on('p2pClose', function(data){
			console.log('user - p2p-close');
			masterSocket.emit('p2pClose', {user: serverID+':'+socket.uid});
		});

		socket.on('p2p-offer', function(data){
			console.log('user - p2p-offer');
			masterSocket.emit('p2p-offer', {user: serverID + ':' + socket.uid, data:data});
		});
		socket.on('p2p-response', function(data){
			console.log('user - p2p-response');
			masterSocket.emit('p2p-response', {user: serverID + ':' + socket.uid, data:data});
		});
		socket.on('p2p-candidate', function(data){
			console.log('user - p2p-candidate');
			masterSocket.emit('p2p-candidate', {user: serverID + ':' + socket.uid, data:data});
		});
	}
	
	//LogOut
	socket.on('keepAlive', function (data) {
		socket.emit('keepAlive');
	});
	
});
/***********************************************************************************************************/



//Gestió dels missatges amb el master
/***********************************************************************************************************/

masterSocket.on('disconnect', function () {
	process.exit(1);
});
//addServer ACK

masterSocket.on('addServer-ack', function (data) {
	serverID = data.id;
	users.setServerId(serverID);
	console.log("master - addServer-ack. serverID: " + serverID);
	
	
	//list
	masterSocket.on('list', function(data){
		console.log('roomsList');
		rooms = data;
		io.sockets.emit('list', data);
	});
	
	//update ACK
	masterSocket.on('update', function(data){
		console.log('update');		
		io.sockets.emit('update', data);
	});
	
	//joinRoom ACK
	masterSocket.on('joinRoom-ack', function(data){
		console.log('joinRoom-ack');
		users.emit(data.user, 'joinRoom-ack', data);
	});

	//joinRoom NACK
	masterSocket.on('joinRoom-nack', function(data){
		console.log('joinRoom-nack', data);
		users.emit(data.user, 'joinRoom-nack', data);
	});

	//createRoom ACK
	masterSocket.on('newRoom-ack', function(data){
		console.log('newRoom-ack');
		users.emit(data.user, 'newRoom-ack', data);
	});

	//createRoom NACK
	masterSocket.on('newRoom-nack', function(data){
		console.log('newRoom-nack', data);
		users.emit(data.user, 'newRoom-nack', data);
	});

	//Login ACK
	masterSocket.on('login-ack', function(data){
		users.login(data.user, data.username);
		users.emit(data.user, 'login-ack', {username:data.username, follow:data.follow, tokens:data.tokens});
		users.emit(data.user, 'list', rooms);
	});

	//Login NACK
	masterSocket.on('login-nack', function(data){
		console.log('login-nack', data);
		users.emit(data.user, 'login-nack', {});
	});

	//Signin ACK
	masterSocket.on('signin-ack', function(data){
		users.login(data.user, data.username);
		users.emit(data.user, 'signin-ack', {username:data.username, tokens:data.tokens});
		users.emit(data.user, 'list', rooms);
	});

	//Signin NACK
	masterSocket.on('signin-nack', function(data){
		console.log('signin-nack', data);
		users.emit(data.user, 'signin-nack', {msg:data.msg});
	});

	//Follow ACK
	masterSocket.on('follow-ack', function(data){
		console.log('follow-ack', data);
		users.emit(data.user, 'follow-ack', data);
	});

	//Follow NACK
	masterSocket.on('follow-nack', function(data){
		console.log('follow-nack', data);
		users.emit(data.user, 'follow-nack', data);
	});

	//unFollow ACK
	masterSocket.on('unfollow-ack', function(data){
		console.log('unfollow-ack', data);
		users.emit(data.user, 'unfollow-ack', data);
	});

	//unFollow NACK
	masterSocket.on('unfollow-nack', function(data){
		console.log('unfollow-nack', data);
		users.emit(data.user, 'unfollow-nack', data);
	});

	masterSocket.on('incTokens-ack', function(data){
		console.log('incTokens-ack', data);
		users.emit(data.user, 'incTokens-ack', data);
	});

	masterSocket.on('incTokens-nack', function(data){
		console.log('incTokens-nack', data);
		users.emit(data.user, 'incTokens-nack', data);
	});

	masterSocket.on('decTokens-ack', function(data){
		console.log('decTokens-ack', data);
		users.emit(data.user, 'decTokens-ack', data);
	});

	masterSocket.on('decTokens-nack', function(data){
		console.log('decTokens-nack', data);
		users.emit(data.user, 'decTokens-nack', data);
	});

	masterSocket.on('sendTokens-nack', function(data){
		console.log('sendTokens-nack', data);
		users.emit(data.user, 'sendTokens-nack', data);
	});

	masterSocket.on('sendTokens-ack', function(data){
		console.log('sendTokens-ack', data);
		users.emit(data.user, 'sendTokens-ack', data);
	});

	masterSocket.on('receivedTokens-ack', function(data){
		console.log('receivedTokens-ack', data);
		users.emit(data.user, 'receivedTokens-ack', data);
	});
	
	
	// eventos para p2p
	masterSocket.on('p2pAsk-nack', function(data){
		console.log('master - p2pAsk-nack', data);
		users.emit(data.user, 'p2pAsk-nack', data);
	});
	masterSocket.on('p2pAsk-ack', function(data){
		console.log('master - p2pAsk-ack', data.msg);
		users.emit(data.user, 'p2pAsk-ack', data);
	});
	
	// eventos para p2p
	masterSocket.on('p2pAsk', function(data){
		console.log('master - p2pAsk', data.msg);
		users.emit(data.user, 'p2pAsk', data);
	});
	
	masterSocket.on('p2p-offer', function(data){
		console.log('master - p2p-offer');//, data);
		users.emit(data.user, 'p2p-offer', data.data);
	});
	masterSocket.on('p2p-response', function(data){
		console.log('master - p2p-response');//, data);
		users.emit(data.user, 'p2p-response', data.data);
	});
	masterSocket.on('p2p-candidate', function(data){
		console.log('master - p2p-candidate');//, data);
		users.emit(data.user, 'p2p-candidate', data.data);
	});
	masterSocket.on('p2pClose', function(data){
		console.log('master - p2p-end');//, data);
		users.emit(data.user, 'p2pClose', data.data);
	});


});

