//Declaració de variables
/***********************************************************************************************************/
var tools = require('./lib/globals.js'),
    Room = require('./lib/broadcast/room.js'),
    VideoProxy = require('./lib/broadcast/videoProxy.js'),
    Generator = require('./lib/broadcast/passgenerator.js'),
    express = require('express'),
    app = express(),
    fs = require('fs'),
    PORT = process.env.PORT || BROADCAST_START_RANGE,
    PORTPROXY = process.env.PORTPROXY || BROADCAST_PROXY_START_RANGE,
    httpsOptions = {
        key: fs.readFileSync('./ssl/broadcaster/broadcaster_key.pem'),
        cert: fs.readFileSync('./ssl/broadcaster/broadcaster_cert.crt'),
        ca: fs.readFileSync('./ssl/ca/ca_cert.crt'),
        requestCert: true,
        rejectUnauthorized: false,
        passphrase: "broadcaster"
    },
    // variables globals del server
    //per guardar les rooms
    rooms = new Array(),
    //per guardar els videoproxies
    serverID,
    passGenerator = new Generator(1234);
	
	
    var httpsServer = require('https').createServer(httpsOptions,app),
    httpsProxyServer = require('https').createServer(httpsOptions,app),
    ioClient = require('socket.io').listen(httpsServer),
    proxyClient = require('socket.io').listen(httpsProxyServer),
    masterSocket = require('socket.io-client').connect('https://' + MASTER_DN +':'+ BROADCAST_MASTER_PORT);
/***********************************************************************************************************/




//Inicialització del servidor
/***********************************************************************************************************/
httpsServer.listen(PORT, function(){
    console.log('Listening on ssl server on port ' + PORT + '...');
})
httpsProxyServer.listen(PORTPROXY, function(){
    console.log('Listening on ssl server on port ' + PORTPROXY + '...');
})

ioClient.configure(function() {
    ioClient.disable('log');
    ioClient.set('transports', ['websocket', 'xhr-polling']);
})
proxyClient.configure(function() {
    proxyClient.disable('log');
    proxyClient.set('transports', ['websocket', 'xhr-polling']);
})

tools.getNetworkIP(function (error, ip) {
    if(!error){
        if(DEBUG) masterSocket.emit('addServer', {ip: 'https://' + BROADCAST_DN, port: PORT, proxyport:PORTPROXY}); //immediatament registrem proxy de video al master      
        else masterSocket.emit('addServer', {ip: ip[0], port: PORT, proxyport:PORTPROXY}); //immediatament registrem proxy de video al master              
    }
    else{
        console.log("Error iniciant servidor de broadcast...");
    }
}, false);
/***********************************************************************************************************/




//Gestió dels missatges amb els clients que retransmeten
/***********************************************************************************************************/
ioClient.sockets.on('connection', function (socket) {
    console.log('client - connect');
    socket.on('newRoom', function (data) {
        tools.print_hash('client - newRoom. Data: ', data);
        // el master ens ha avisat de la sala?
        if ((typeof data.room !== "undefined") && (typeof rooms[data.room] !== "undefined")) {
            console.log(data);
            console.log(rooms[data.room]);
            // el usuari i contrasenya correctes
            if (data.user == rooms[data.room].creator.user && data.password == rooms[data.room].password) {
                //ok afegim el socket a la room
                rooms[data.room].creator.socket = socket;
                socket.room = data.room;
                socket.emit('newRoom-ack', { room: data.room });

                console.log('Enviem ack de new room a client...');

                // al rebre un nou frame
                socket.on('newFrame', function (data) {
                    if (typeof rooms[socket.room] !== "undefined") {
                        if (rooms[socket.room].lastFrame <= 0) {
                            rooms[socket.room].lastFrame = FRAMES_PER_CAPTURE;
                            masterSocket.emit('RoomFrame', { room: socket.room, frame: data });
                        }
                        rooms[socket.room].lastFrame--;
                        proxyClient.sockets.in(socket.room).emit('newFrame', data);
                    }
                });
				
                // esta pausat
                socket.on('paused', function () {
                    if (typeof rooms[socket.room] !== "undefined") {
                        if (rooms[socket.room].lastPause <= 0) {
                            rooms[socket.room].lastPause = FRAMES_PER_CAPTURE;
                            masterSocket.emit('RoomPaused', { room: socket.room});
                        }
                        rooms[socket.room].lastPause--;
                        rooms[socket.room].lastFrame=0;
                        proxyClient.sockets.in(socket.room).emit('paused');
                    }
                });

                socket.on('chatMsg', function (data) {
                    proxyClient.sockets.in (socket.room).emit('chatMsg', {user:rooms[socket.room].creator.username, level:'creator', msg:data});
                    socket.emit('chatMsg', {user:rooms[socket.room].creator.username, level:'creator', msg:data});
                });
                
                // al avis sala
                socket.on('endconnection', function (data) {
                    console.log('client - endconnection');
                    masterSocket.emit('RoomClose', { room: socket.room });
                    proxyClient.sockets.in(socket.room).emit('RoomClose', { room: socket.room });
                });
                // al tancar sala
                socket.on('disconnect', function (data) {
                    console.log('client - disconnect');
                    masterSocket.emit('RoomClose', { room: socket.room });
                    proxyClient.sockets.in(socket.room).emit('RoomClose', { room: socket.room });
                });

            } else {
                socket.emit('newRoom-nack', { errorInfo: 'Usuari o contraseña incorrectes' })
            }
        } else {
            socket.emit('newRoom-nack', { errorInfo: 'roomID erroni' })
        }
    });
});
/***********************************************************************************************************/



//Gestió dels missatges amb els clients que retransmeten
/***********************************************************************************************************/
proxyClient.sockets.on('connection', function (socket) {
    console.log('proxy - connect');
    socket.on('getFrames', function (data) {
        tools.print_hash('videoProxy - getFrames. Data: ', data);
        if (typeof rooms[data.room].futureVideoProxies[data.videoProxy] !== 'undefined') {
            socket.join(data.room);
            socket.room = data.room;
            rooms[data.room].videoProxies[data.videoProxy] = data.videoProxy;
            delete rooms[data.room].futureVideoProxies[data.videoProxy];

            socket.on('chatMsg', function (data) {
                proxyClient.sockets.in (socket.room).emit('chatMsg', {user:data.username, level:data.level, msg:data.msg});
                rooms[socket.room].creator.socket.emit('chatMsg', {user:data.username, level:data.level, msg:data.msg});
            });
        }
    });
});
/***********************************************************************************************************/




//Gestió dels missatges amb el master
/***********************************************************************************************************/
masterSocket.on('addServer-ack', function(data){
    serverID = data.id;
    console.log("master - addServer-ack. serverID: " + serverID);
});

//conexió amb el master
masterSocket.on('connect', function(){
	console.log('Broadcast obre socket automàticament amb el master...');
})  

masterSocket.on('disconnect', function () {
	process.exit(1);
});

//rebem una petició de nova room del master
masterSocket.on('newRoom', function(data){
	tools.print_hash('master - newRoom. Data: ', data);
	console.log('Received newRoom event')
	//l'afegim a l'estructura de dades per quan l'usuari es conecti
    password = passGenerator.nextPass();
    
	rooms[data.room] = new Room(data.room, data.name, data.user, data.creator, password);
	tools.print_hash('master - newRoom. rooms: ', rooms);
	masterSocket.emit('newRoom-ack', { user: data.user, password : password, server: data.server, room: data.room})
})


masterSocket.on('newProxy', function(data){
	tools.print_hash('master - newProxy. Data: ', data);
    if(!rooms[data.room]){  //si no existeix la room
        //MAL ROLLITO...
        console.log('Mal rollito, la sala no existeix...');
    }
    else{
        rooms[data.room].addFutureVideoProxy(data.videoProxy);
		tools.print_hash('master - newProxy. rooms: ', rooms);
        masterSocket.emit('newProxy-ack', {user: data.user, server: data.server, room: data.room, videoProxy: data.videoProxy, password:data.password, type: data.type});
    }
})
/***********************************************************************************************************/
