/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlRootElement;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author nyanco
 */
@XmlRootElement(name = "Room")
class Room implements Serializable {
    public int id;
    public String roomName;
    public String creator;
    public String gen;
    public String frame;
    public boolean isPrivate;
    public int tokens;
    public boolean allowUnregistered;

    Room() {}
    
    
    
}