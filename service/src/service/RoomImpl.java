/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONException;
import org.json.JSONObject;


public class RoomImpl extends Room {
    RoomImpl(JSONObject o) {
        try {
            id = o.getInt("id");
            roomName = o.getString("roomName");
            creator = o.getString("creator");
            gen = o.getString("gen");
            String type = o.getString("type"); 
            isPrivate = type.equalsIgnoreCase("private") || type.equalsIgnoreCase("private_token"); 
            tokens = o.getInt("tokens");
            allowUnregistered = o.getBoolean("allowUnregistered");
        } catch (JSONException ex) {
            Logger.getLogger(Room.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}
