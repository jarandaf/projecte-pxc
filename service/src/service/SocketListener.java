/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import io.socket.IOAcknowledge;
import io.socket.IOCallback;
import io.socket.SocketIO;
import io.socket.SocketIOException;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author nyanco
 */
public class SocketListener implements IOCallback  {
    SocketIO socket;
    RoomsViewer rooms;
    
    public SocketListener(String url, RoomsViewer rooms) throws Exception {
        this.socket = new SocketIO();
        this.rooms = rooms;
        this.socket.connect(url,this);       
    }

    @Override
    public void onDisconnect() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void onConnect() {
        System.out.println("connected");
    }

    @Override
    public void onMessage(String string, IOAcknowledge ioa) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void onMessage(JSONObject jsono, IOAcknowledge ioa) {
        try {
            System.out.println("Server said:" + jsono.toString(2));
        } catch (JSONException ex) {
            Logger.getLogger(SocketListener.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void on(String string, IOAcknowledge ioa, Object... os) {
        switch(string){
            case "list":
                HashMap<Integer, Room> newRooms = new HashMap<>();
                JSONArray list = (JSONArray) os[0];
                for(int i=0; i<list.length(); i++) {
                    try {
                        JSONObject o = list.getJSONObject(i);
                        Room r = new RoomImpl(o);
                        newRooms.put(r.id, r);
                    } catch (JSONException ex) {
                        Logger.getLogger(SocketListener.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
                rooms.replace(newRooms);
                break;
            case "update":
                JSONObject o = (JSONObject) os[0];
                Room r = new RoomImpl(o);
                rooms.add(r);
                break;
        }
    }

    @Override
    public void onError(SocketIOException sioe) {
        System.err.println(sioe);
    }
            
}
