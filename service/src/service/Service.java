/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.jws.WebMethod;
import javax.jws.WebService;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.transform.Result;
import javax.xml.transform.sax.SAXResult;
/**
 *
 * @author nyanco
 */
@WebService
public class Service {
    
    RoomsViewer rooms;
    SocketListener socketListener;

    @WebMethod
    public String[] users(){
        return rooms.getUsers();
    }
    
    @WebMethod
    public Integer[] user(String user) {
        return rooms.search(user);
    }
    
    @WebMethod
    public Integer[] rooms(String user) {
        return rooms.all();
    }
    
    
    public Service(String url) {
        try {
            rooms = new RoomsViewer();
            socketListener = new SocketListener(url, rooms);
        } catch (Exception ex) {
            Logger.getLogger(Service.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
