/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

/**
 *
 * @author nyanco
 */
class RoomsViewer {
    HashMap<Integer, Room> rooms;
    HashMap<String, ArrayList<Integer>> creator2Room;

    public RoomsViewer(){
        rooms = new HashMap<>();
        creator2Room = new HashMap<>();
    }
    
    void disconnected() {
        throw new UnsupportedOperationException("Not yet implemented");
    }

    void replace(HashMap<Integer, Room> newRooms) {
        rooms = newRooms;
        Iterator<Integer> it = rooms.keySet().iterator();
        HashMap<String, ArrayList<Integer>> newCreator2Room = new HashMap<>();
        while(it.hasNext()){
            Integer id = it.next();
            Room r = rooms.get(id);
            addToList(newCreator2Room, r.creator, id);
        }
        creator2Room = newCreator2Room;
    }

    void add(Room r) {
        rooms.put(r.id, r);
        addToList(creator2Room, r.creator, r.id);
    }

    private void addToList(HashMap<String, ArrayList<Integer>> map, String key, Integer value) {
        ArrayList<Integer> list = map.get(key);
        if(list==null) {
            list = new ArrayList<>();
        }
        list.add(value);
        map.put(key, list);
    }

    String[] getUsers() {
        return (String[]) creator2Room.keySet().toArray();
    }

    Integer[] search(String key) {
        return (Integer[]) creator2Room.get(key).toArray();
        
    }

    Integer[] all() {
        return (Integer[]) rooms.keySet().toArray();
    }
    
}
